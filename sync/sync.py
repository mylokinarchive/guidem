import ftplib
import sys
import os
import optparse
import logging

logging.basicConfig(level=logging.INFO)
sys.path.append(os.getcwd())


HOST = "1.mylokin.cz8.ru"
LOGIN = "mylokin"
PWD = "s1ZnvXJaig2L"
PATH = "/app/guidem/public/music/"

def sync_folder(option, opt, value, parser):
    path = value #optparser adoptation
    
    if os.access(path, os.R_OK):
        directory = os.path.basename(os.path.dirname(path))#TODO fix last slash adoptation
        directory = os.path.join(PATH, directory)
        ftp = get_ftp_connection()
        ftp.cwd(PATH)
        try:
            ftp.mkd(directory)
        except ftplib.error_perm:
            logging.error("Cannot create folder '%s'. Already exist?"%directory)
        ftp.cwd(directory)
        
        for root, dirs, files in os.walk(path):
            root = os.path.join(os.getcwd(), root)
            for fname in files:
                f = open(os.path.join(root, fname), 'rb')
                ftp.storbinary("STOR %s"%fname, f)
                logging.info("Store file '%s'"%fname)
                
    else:
        logging.error("Cannot grant access to folder '%s'"%os.path.basename(path))


def get_ftp_connection():
    ftp = ftplib.FTP()
    ftp.connect(HOST)
    ftp.login(LOGIN, PWD)
    return ftp

parser = optparse.OptionParser()
parser.add_option('--folder', action="callback", callback=sync_folder, type="string")

if __name__=="__main__":
    options, args = parser.parse_args()
