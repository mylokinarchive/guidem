This file is for you to describe the guidem application. Typically
you would include information such as the information below:

Installation and Setup
======================

Install ``guidem`` using easy_install::

    easy_install guidem

Make a config file as follows::

    paster make-config guidem config.ini

Tweak the config file as appropriate and then setup the application::

    paster setup-app config.ini

Then you are ready to go.
