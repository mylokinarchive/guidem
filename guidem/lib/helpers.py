# -*- coding: utf-8 -*-
"""Helper functions

Consists of functions to typically be used within templates, but also
available to Controllers. This module is available to templates as 'h'.
"""
import lxml.html

from routes import url_for
import urlparse, urllib, cgi
import auth

import webhelpers.paginate as paginate

from guidem.model import Bookmark, Account, now

def path(request):
    return urlparse.urlparse(request.path).path

def is_bookmarked(page, username):
    if Bookmark.is_bookmarked(page, username):
        bookmark = Bookmark.get_unique(page, username)
        bookmark.datetime = now()()
        bookmark.db_add()
        return True
    else:
        return False

def get_account(username):
    return Account.get(username)

def updates(account):
    return Bookmark.updates(account)

def account_update(username):
    Account.get(username).update()

def mark_as_spamer(comment):
    account = Account.get(comment.username)
    account.spamer = True
    return account.db_add()

def title(text):
    return text.title()

def url_query_edit(url, param, val):
    url = list(urlparse.urlsplit(url))
    query = dict(cgi.parse_qsl(url[3]))
    query[param] = val
    query = urllib.urlencode(query)
    url[3] = query
    return urlparse.urlunsplit(url)
    
def text_content(text):
    t = lxml.html.fromstring(text)
    return t.text_content()

def sub(text, n):
    gen = (word for word in text.split())
    text = ''
    for word in gen:
        if len('%s %s'%(text, word))>n:
            return '%s...'%text
        else:
            text = '%s %s'%(text, word)
    return text
            
def adopt(n, words):
    #новость, новости, новостей
    assert len(words)==3, u"Wrong words list size"
    
    adopt_dict = {
        0:2,
        1:0,
        2:1,
        3:1,
        4:1,
        5:2,
        6:2,
        7:2,
        8:2,
        9:2,
        10:2,
        11:2,
        12:2,
        13:2,
        14:2,
        15:2,
        16:2,
        17:2,
        18:2,
        19:2
        }
    
    n = str(n)[-2:]
    if len(n)==1 or int(n) in adopt_dict.keys():
        return words[adopt_dict[int(n)]]
    else:
        return words[adopt_dict[int(n[-1])]]
    
