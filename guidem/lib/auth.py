from pylons.controllers.util import abort, redirect_to
from pylons.decorators.util import get_pylons
from decorator import decorator
import Cookie
import guidem.model

class Users(object):
    accounts = guidem.model.Account
    
    @staticmethod
    def user_exists(username):
        return True if guidem.model.Account.get(username) else False
    
    @staticmethod
    def user_add_permission(username, permission):
        return guidem.model.Permission(username, permission).db_add()

    @staticmethod
    def user_has_permission(username, permission):
        permissions = [perm.permission for perm in guidem.model.Permission.get(username = username)]
        return True if permission in permissions else False

    @staticmethod
    def user_create(username, password, email, permissions=tuple()):
        username = username.lower()
        if not Users.user_exists(username):
            account = guidem.model.Account(username, password, email).db_add()
            if account:
                for perm in permissions:
                    Users.user_add_permission(username, perm)
                return account
            else:
                return False
        else:
            return False

def authentication(func, self, *args, **kwargs):
    environ = get_pylons(args).request.environ
    if not environ['_auth']:
        return redirect_to('signin')
    return func(self, *args, **kwargs)

authentication = decorator(authentication)

def authorization(*permissions):
    def wrapper(func):
        def protected(*args, **kwargs):
            environ = kwargs['environ']
            if not environ['_auth']:
                return redirect_to('signin')            
            for perm in permissions:
                if perm not in map(str, environ['_auth'].permissions):
                    return abort(403)
            return func(*args)
        return protected
    return wrapper

def parse_cookies(cookies):
    rawdata = Cookie.BaseCookie()
    rawdata.load(cookies)
    return dict([(key, value.value) for key, value in rawdata.items()])

def parse_auth_cookie(cookie):
    username, password = cookie[1:-1].split(':')
    return username, password
    
class Permissions(dict):
    def __getitem__(self, index):
        try:
            super(Permissions, self).__getitem__(index)
        except KeyError:
            return False
        else:
            return True

class AuthMiddleware(object):
    def __init__(self, app):
        self.app = app
        
    def __call__(self, environ, start_response):
        environ['_auth'] = None
        environ['_perm'] = Permissions()
        if environ.has_key('HTTP_COOKIE'):
            cookies = parse_cookies(environ['HTTP_COOKIE'])
            if cookies.has_key('_auth'):
                username, password = parse_auth_cookie(cookies['_auth'])
                account = guidem.model.Account.get(username)
                if account and account.password==password:
                    environ['_auth'] = account
                    environ['_auth'].activity()
                    environ['_perm'].update(dict([(str(perm), True) for perm in account.permissions]))
        elif environ.has_key('TESTING'):
            account = guidem.model.Account.get(u'admin')
            environ['_auth'] = account
            environ['_perm'].update(dict([(str(perm), True) for perm in account.permissions]))            
        return self.app(environ, start_response)

#Copyright 2010 Andrey Gubarev