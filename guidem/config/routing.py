"""Routes configuration

The more specific and detailed routes should be defined first so they
may take precedent over the more generic routes. For more information
refer to the routes manual at http://routes.groovie.org/docs/
"""
from pylons import config
from routes import Mapper

def make_map():
    """Create, configure and return the routes Mapper"""
    map = Mapper(directory=config['pylons.paths']['controllers'],
                 always_scan=config['debug'])
    map.minimization = False

    map.connect('/error/{action}', controller='error')
    map.connect('/error/{action}/{id}', controller='error')
    
    #Base
    map.connect('home', '/', controller='base', action='home')
    map.connect('tracks', '/tracks', controller='base', action='tracks')
    map.connect('tracks_letter', '/tracks/{letter}', controller='base', action='tracks_letter')
    map.connect('discussions', '/discussions', controller='base', action='discussions')
    map.connect('artists', '/artists', controller='base', action='artists')
    map.connect('artists_letter', '/artists/{letter}', controller='base', action='artists_letter')
    map.connect('timeline', '/timeline', controller='base', action='timeline')
    map.connect('timeline_accounts', '/timeline/accounts', controller='base', action='timeline_accounts')
    map.connect('timeline_tracks', '/timeline/tracks', controller='base', action='timeline_tracks')
    map.connect('timeline_artists', '/timeline/artists', controller='base', action='timeline_artists')
    map.connect('feed', '/feed', controller='base', action='feed')
    map.connect('player', '/player', controller='base', action='player')
    map.connect('/player/track/{id}', controller='base', action='track_info')
    map.connect('/player/track/{id}/render', controller='base', action='track_render')
    map.connect('/player/track/{id}/render_history', controller='base', action='track_history_render')
    map.connect('radio', '/radio', controller='base', action='radio')
    map.connect('composition', '/composition/{tid}', controller='base', action='composition')
    
    #Account
    map.connect('account', '/account', controller='account', action='account')
    map.connect('updates', '/account/updates', controller='account', action='updates')
    map.connect('read', '/account/read', controller='account', action='read_bookmark')

    map.connect('acc_pm', '/account/pm', controller='account', action='pm')
    map.connect('acc_bookmarks', '/account/bookmarks', controller='account', action='bookmarks')
    map.connect('acc_friends', '/account/friends', controller='account', action='friends')
    map.connect('acc_ignored', '/account/ignored', controller='account', action='ignored')
    map.connect('acc_requests', '/account/requests', controller='account', action='requests')
    map.connect('acc_playlists', '/account/playlists', controller='account', action='playlists')

    map.connect('add_to_playlist', '/account/playlist/add', controller='account', action='add_to_playlist')
    map.connect('remove_from_playlist', '/account/playlist/remove_track', controller='account', action='remove_from_playlist')        
    map.connect('create_playlist', '/account/playlist/create', controller='account', action='create_playlist')    
    map.connect('create_playlist_submit', '/account/playlist/create/submit', controller='account', action='create_playlist_submit')        
    map.connect('remove_playlist', '/account/playlist/remove', controller='account', action='remove_playlist')    
    
    map.connect('info', '/account/info', controller='account', action='info')
    map.connect('info_submit', '/account/info/submit', controller='account', action='info_submit')
    map.connect('correspondence', '/account/correspondence/{username}', controller='account', action='correspondence')
    map.connect('read_updates', '/account/updates/read', controller='account', action='read_updates')
    map.connect('show_account', '/account/show/{username}', controller='account', action='show')
    map.connect('friends_timeline', '/account/friends_timeline', controller='account', action='friends_timeline')
    map.connect('reset_password', '/account/reset_password', controller='account', action='reset_password')	
    map.connect('reset_password_submit', '/account/reset_password/submit', controller='account', action='reset_password_submit')	
    map.connect('register', '/account/register', controller='account', action='register')	
    map.connect('register_submit', '/account/register/submit', controller='account', action='submit')		
    map.connect('signin', '/account/signin', controller='account', action='signin')
    map.connect('signin_submit', '/account/signin/submit', controller='account', action='signin_submit')
    map.connect('signout', '/account/signout', controller='account', action='signout')
    #Admin
    map.connect('admin', '/admin', controller='admin', action='home')
    map.connect('admin_stream', '/admin/stream', controller='admin', action='stream')    
    map.connect('admin_images', '/admin/images', controller='admin', action='images')    
    map.connect('set_admin_role', '/admin/set_admin_role', controller='admin', action='set_admin_role')
    #Admin remove
    map.connect('block', '/admin/block', controller='admin', action='block')
    map.connect('unblock', '/admin/unblock', controller='admin', action='unblock')
    map.connect('remove_song', '/admin/remove/song', controller='admin', action='remove_song')
    map.connect('remove_artist', '/admin/remove/atist', controller='admin', action='remove_artist')
    map.connect('remove_image', '/admin/remove/image', controller='admin', action='remove_image')
    map.connect('remove_comment', '/admin/remove/comment', controller='admin', action='remove_comment')
    map.connect('remove_discus', '/admin/remove/discus', controller='admin', action='remove_discus')
    #Admin add content
    map.connect('master_track', '/master/track', controller='admin', action='master_track')
    map.connect('master_track_submit', '/master/track/submit', controller='admin', action='master_track_submit')
    map.connect('master_track_url_submit', '/master/track/submit_url', controller='admin', action='master_track_url_submit')
    map.connect('confirm_track', '/admin/confirm', controller='admin', action='confirm_track')
    
    map.connect('add_news', '/admin/add/news', controller='admin', action='add_news')
    map.connect('add_news_submit', '/admin/add/news/submit', controller='admin', action='add_news_submit')
    map.connect('add_image', '/admin/add/image', controller='admin', action='add_image')
    map.connect('add_image_submit', '/admin/add/image/submit', controller='admin', action='add_image_submit')
    #Admin edit content
    map.connect('edit_artist', '/admin/edit/artist', controller='admin', action='edit_artist')
    map.connect('edit_artist_submit', '/admin/edit/artist/submit', controller='admin', action='edit_artist_submit')
    map.connect('edit_track', '/admin/edit/track', controller='admin', action='edit_track')
    map.connect('edit_track_submit', '/admin/edit/track/submit', controller='admin', action='edit_track_submit')
    map.connect('edit_discus', '/admin/edit/discus', controller='admin', action='edit_discus')
    map.connect('edit_discus_submit', '/admin/edit/discus/submit', controller='admin', action='edit_discus_submit')
    map.connect('edit_news', '/admin/edit/news', controller='admin', action='edit_news')
    map.connect('edit_news_submit', '/admin/edit/news/submit', controller='admin', action='edit_news_submit')
    #File
    map.connect('track', '/file/track/{id}.mp3', controller='file', action='track')
    map.connect('image', '/file/img/{id}', controller='file', action='image')    
    #Page
    map.connect('song', '/song/{id}', controller='page', action='song')
    map.connect('artist', '/artist/{id}', controller='page', action='artist')
    map.connect('discus', '/discus/{id}', controller='page', action='discussion')
    map.connect('news', '/news/{id}', controller='page', action='news')
    map.connect('search', '/search', controller='page', action='search')
    map.connect('playlist', '/playlist/{username}/{title}', controller='page', action='playlist')
    map.connect('playlistn', '/playlist/{id}', controller='page', action='playlistn')#TODO 
    #Social
    map.connect('pm', '/social/pm', controller='communication', action='pm')
    map.connect('invite', '/social/invite', controller='communication', action='invite')
    map.connect('invite_submit', '/social/invite/submit', controller='communication', action='invite_submit')
    map.connect('add_comment', '/social/add_comment', controller='communication', action='add_comment')
    map.connect('add_discussion', '/social/add_discussion', controller='base', action='add_discussion')
    map.connect('remove_myself_comment', '/social/remove_comment', controller='communication', action='remove_comment')
    map.connect('add_bookmark', '/social/add_bookmark', controller='communication', action='add_bookmark')
    map.connect('remove_bookmark', '/social/remove_bookmark', controller='communication', action='remove_bookmark')
    map.connect('add_friend', '/social/add_friend', controller='communication', action='add_friend')
    map.connect('remove_friend', '/social/remove_friend', controller='communication', action='remove_friend')
    map.connect('ignore', '/social/ignore', controller='communication', action='ignore')
    map.connect('mark_as_spam', '/social/mark_as_spam', controller='communication', action='mark_as_spam')   
    return map
