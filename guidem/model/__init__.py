# -*- coding: utf-8 -*-
"""The application's model objects"""
import os
import logging
import boto
import sqlalchemy as sa
from sqlalchemy import orm
from sqlalchemy.databases import mysql
from guidem.model import meta
from pylons.templating import render_mako_def

import datetime

from routes import url_for
from urlparse import urlparse

log = logging.getLogger(__name__)
now = lambda: datetime.datetime.now
week_ago = lambda: datetime.datetime.now()-datetime.timedelta(7)

def init_model(engine):
    """Call me before using any of the tables or classes in the model"""
    meta.Session.configure(bind=engine)
    meta.engine = engine





################################################################################
#ORM model

class OrmObject(object):
    @classmethod
    def get_by_id(cls, id):
        try:
            return meta.Session.query(cls).filter(cls.id==id).one()
        except:
            return None

    @classmethod
    def get(cls, **filters):
        query = meta.Session.query(cls)
        for column in filters:
            field = getattr(cls, column)
            query = query.filter(field==filters[column])
        return query
    
    def db_add(self):
        try:
            meta.Session.add(self)
            meta.Session.commit()
            return self
        except:
            meta.Session.rollback()
            return False
    
    def db_remove(self):
        try:
            meta.Session.delete(self)
            meta.Session.commit()
            return True
        except:
            meta.Session.rollback()
            return False
    
    
    


################################################################################
#File model
    
table_file = sa.schema.Table('file', meta.metadata,
    sa.schema.Column('id', sa.types.Integer(), primary_key=True),
    sa.schema.Column('filename', sa.types.Unicode(100)),
    sa.schema.Column('path', sa.types.Unicode(100), nullable=False, unique=True),
    sa.schema.Column('contentlength', sa.types.Integer(), nullable=False),
    )

        
class File(OrmObject):
    def __init__(self, filename, path, contentlength):
        self.filename = filename
        self.path = path
        self.contentlength = contentlength
        
    def full_remove(self):
        if self.path[:4]==u"aws:":
            conn = boto.connect_s3('AKIAJ5TBK4KX3L3HJE3A', 'KlMgimVql5cTAuRMp8jdCrvWr+9Q7+tOEzkO9wam')
            key = conn.get_bucket('guidem').get_key(self.path[4:])
            if key:
                key.delete()
        else:
            try:
                os.remove(self.path.encode('utf-8'))
            except:
                pass
        return True if self.db_remove() else False

orm.mapper(File, table_file)






################################################################################
#Artist model

table_artist = sa.schema.Table('artist', meta.metadata,
    sa.schema.Column('id', sa.types.Integer(), primary_key=True),
    sa.schema.Column('name', sa.types.Unicode(100), nullable=False, unique=True),
    sa.schema.Column('who', sa.types.Unicode(20)),
    sa.schema.Column('info', sa.types.UnicodeText()),
    sa.schema.Column('datetime', sa.types.DateTime(), default=now()),
    )

class Artist(OrmObject):
    def __init__(self, name, info=u''):
        self.name = name.lower()
        self.info = info
        
    @classmethod
    def get_or_add(cls, name):
        artist = cls.get(name=name.lower())
        return artist.one() if artist.count()==1 else cls(name).db_add()
        
    @property
    def count_tracks(self):
        return meta.Session.query(Track).filter(Track.artist==self.id).count()

    def full_remove(self):
        page = urlparse(url_for('artist', id=self.id)).path
        if Comment.remove_from_page(page) and Bookmark.remove_for_page(page):
            for track in self.tracks:
                track.full_remove()
            return self.db_remove()
        else:
            return False   

orm.mapper(Artist, table_artist)






################################################################################
#Track model
    
table_track = sa.schema.Table('track', meta.metadata,
    sa.schema.Column('id', sa.types.Integer(), primary_key=True),
    sa.schema.Column('title', sa.types.Unicode(100), nullable=False),
    sa.schema.Column('artist', sa.types.Integer(), sa.schema.ForeignKey('artist.id'), nullable=False),
    sa.schema.Column('datetime', sa.types.DateTime(), default=now()),
    sa.schema.Column('username', sa.types.Unicode(100)),
    sa.schema.Column('file', sa.types.Integer(), sa.schema.ForeignKey('file.id')),

    sa.schema.Column('composition', sa.types.Unicode(50)),
    sa.schema.Column('no', sa.types.Integer()),

    sa.schema.UniqueConstraint('title', 'artist', name='unique_track'),
    sa.schema.UniqueConstraint('artist', 'composition', 'no', name='unique_composition'),
    ) 

table_track_file = sa.schema.Table('trackfile', meta.metadata,
    sa.schema.Column('id', sa.types.Integer(), primary_key=True),
    sa.schema.Column('file', sa.schema.ForeignKey('file.id'), nullable=False),
    sa.schema.Column('track', sa.schema.ForeignKey('track.id'), nullable=False),
    )

class Track(OrmObject):
    def __init__(self, title, artist, file, composition=None, no=0, username=None):
        self.title = title.lower()
        self.artist = artist.id
        self.file = file
        self.composition = composition
        self.no = no

        self.__repr__ = '%s %s'%(title, artist.name)
        self.username = username

    def render(self):
        return render_mako_def("timeline_widgets.html", "track", track=self)

    def render_one(self):
        return render_mako_def("timeline_widgets.html", "track_one", track=self)        
        
    def full_remove(self):
        page = urlparse(url_for('song', id=self.id)).path
        if Comment.remove_from_page(page) and Bookmark.remove_for_page(page):
            self.fileobj.full_remove()
            for playlist in self.playlists:
                playlist.db_remove()
            return self.db_remove()
        else:
            return False
    
    def next_by_composition(self):
        track = meta.Session.query(Track).filter(Track.composition==self.composition)\
            .filter(Track.no>self.no).order_by(Track.no).first()
        return track
    
    def confirm(self):
        self.username = u''
        return self.db_add()

orm.mapper(Track, table_track, properties={
    'artistobj': orm.relation(Artist, backref='tracks'),
    'fileobj': orm.relation(File, backref='tracks') 
    })


################################################################################
#Image model

table_image = sa.schema.Table('image', meta.metadata,
    sa.schema.Column('id', sa.types.Integer(), primary_key=True),
    sa.schema.Column('title', sa.types.Unicode(100)),
    sa.schema.Column('description', sa.types.UnicodeText),
    sa.schema.Column('file', sa.types.Integer(), sa.schema.ForeignKey('file.id'), nullable=False),
    )

class Image(OrmObject):
    def __init__(self, title, description, file):
        self.title = title
        self.description = description
        self.file = file.id
    
    @property
    def content(self):
        return self.fileobj.content

orm.mapper(Image, table_image, properties={
    'fileobj': orm.relation(File, backref='images') 
    })






################################################################################
#Page model

table_page = sa.schema.Table('page', meta.metadata,
    sa.schema.Column('id', sa.types.Integer(), primary_key=True),
    sa.schema.Column('type', sa.types.Unicode(100), nullable=False),
    sa.schema.Column('relative', sa.types.Unicode(100), nullable=False),
    sa.schema.UniqueConstraint('type', 'relative', name='unique_page'),
    )


class Page(OrmObject):
    def __init__(self, type, relative):
        self.type = type
        self.relative = relative

    @property
    def relativeobj(self):
        obj = None
        if self.type==u'track':            
            obj = Track
        elif self.type==u'artist':
            obj = Artist
        elif self.type==u'discussion':
            obj = Discus
        elif self.type==u'news':
            obj = News
        elif self.type==u'playlist':
            obj = Playlist
        return obj.get_by_id(self.relative)
    
    @property
    def title(self):
        title = ''
        if self.type==u'track':            
            title = self.relativeobj.title
        elif self.type==u'artist':
            title = self.relativeobj.name
        elif self.type==u'discussion':
            title = self.relativeobj.title
        elif self.type==u'news':
            title = self.relativeobj.title
        elif self.type==u'playlist':
            title = self.relativeobj.title
        return title

    @property
    def type_str(self):
        if self.type==u'track':            
            return u'Песня'
        elif self.type==u'artist':
            return u'Исполнитель'
        elif self.type==u'discussion':
            return u'Дискуссия'
        elif self.type==u'news':
            return u'Новость'
        elif self.type==u'playlist':
            return u'Плейлист'
    
    @property
    def path(self):
        controller = 'home'
        if self.type==u'track':            
            controller = 'song'
        elif self.type==u'artist':
            controller = 'artist'
        elif self.type==u'discussion':
            controller = 'discus'
        elif self.type==u'news':
            controller = 'news'
        elif self.type==u'playlist':
            controller = 'playlistn'
        return url_for(controller, id=self.relative)

    @classmethod
    def get_or_add(cls, type, relative):
        page = cls.get(type=type, relative=relative)
        return page.one() if page.count()==1 else cls(type, relative).db_add()    
    
orm.mapper(Page, table_page)






################################################################################
#Discussion model

table_discussion = sa.schema.Table('discussion', meta.metadata,
    sa.schema.Column('id', sa.types.Integer(), primary_key=True),
    sa.schema.Column('title', sa.types.Unicode(100), nullable=False),
    sa.schema.Column('theme', sa.types.Unicode(100), nullable=False),
    sa.schema.Column('datetime', sa.types.DateTime(), default=now()),
    )

class Discus(OrmObject):
    def __init__(self, title, theme):
        self.title = title
        self.theme = theme

    def full_remove(self):
        page = (urlparse(url_for('discus', id=self.id)).path).decode('utf-8')
        if Comment.remove_from_page(page) and Bookmark.remove_for_page(page):
            return self.db_remove()
        else:
            return False 
        
    @property
    def last_comment(self):
        page = Page.get_or_add(u'discussion', self.id)
        comment = Comment.get(page = page.id).order_by(Comment.datetime.desc())
        try:
            return comment[0]
        except IndexError:
            return None

orm.mapper(Discus, table_discussion)






################################################################################
#Account model

table_account = sa.schema.Table('account', meta.metadata,
    sa.schema.Column('id', sa.types.Integer(), primary_key=True),
    sa.schema.Column('username', sa.types.Unicode(100), nullable=False, unique=True),
    sa.schema.Column('password', sa.types.Unicode(100), nullable=False),    
    sa.schema.Column('lastupdate', sa.types.DateTime(), default=now()),    
    sa.schema.Column('datetime', sa.types.DateTime(), default=now()),
    sa.schema.Column('spamer', sa.types.Boolean(), default=False),
    sa.schema.Column('blocked', sa.types.Boolean(), default=False),
    sa.schema.Column('email', sa.types.Unicode(100), nullable=False, unique=True),
    sa.schema.Column('info', sa.types.UnicodeText()),
    )

class Account(OrmObject):
    def __init__(self, username, password, email):
        self.username = username
        self.password = password
        self.email = email

    def render(self):
        return render_mako_def("timeline_widgets.html", "account", account=self)
    
    def render_one(self):
        return render_mako_def("timeline_widgets.html", "account_one", account=self)
        
    @property
    def alive(self):
        try:
            return self.username
        except:
            return False
        
        
    @property
    def online(self):
        if Online.get_or_add(self.username).activity>=now()()-datetime.timedelta(minutes=15):
            return True
        else:
            return False

    def offline(self):
        return Online.get_or_add(self.username).set_offline()
        
    def activity(self):
        return Online.get_or_add(self.username).update()
    
    @property
    def playlists(self):
        return Playlist.get_user_playlists(self.username)
        
    @classmethod
    def get(cls, username):
        account = meta.Session.query(cls).filter(cls.username==username)
        return account.one() if account.count()==1 else None

    @classmethod
    def get_by_email(cls, email):
        account = meta.Session.query(cls).filter(cls.email==email)
        return account
    
    @classmethod
    def try_to_get(cls, username):
        account = cls.get(username=username)
        return account.one() if account.count()==1 else False

    
    def update(self):
        self.lastupdate = now()()
        return self.db_add()
    
    #Friends    
    @property
    def friends(self):
        return [friend for friend in Friends.friends_to(self.username)]

    @property
    def ignored(self):
        return [friendship.username2 for friendship in Friends.ignored(self.username)]
        
    @property
    def friendship_requests(self):
        return [friend for friend in Friends.requests(self.username)]
    
    def is_friend(self, username):
        return Friends.is_friends(self.username, username)

    #PM
    @property
    def unread(self):
        return PM.unread_count(self.username)

    @property
    def pm(self):
        return PM.to(self.username)
    
    #Bookmark
    
    @property
    def bookmarks(self):
        return Bookmark.user(self.username)

    #Timeline
    @property
    def timeline_comments(self):        
        return Comment.get(username=self.username).group_by(Comment.page)

    @property
    def timeline_bookmarks(self):        
        return Bookmark.get(username=self.username)       

    @property
    def timeline_friends_comments(self):
        #TODO need better solution
        comments = []
        for friend in self.friends:
            friend = Account.get(friend)
            comments = comments.union(friend.timeline_comments) if comments else friend.timeline_comments
        return comments

    @property
    def timeline_friends_bookmarks(self):
        #TODO need better solution
        bookmarks = []
        for friend in self.friends:
            friend = Account.get(friend)
            bookmarks = bookmarks.union(friend.timeline_bookmarks) if bookmarks else friend.timeline_bookmarks
        return bookmarks    
    
    @staticmethod
    def order_comments_by_datetime(query):
        if query:
            return query.order_by(Comment.datetime.desc())
        else:
            return query

    @staticmethod
    def order_bookmarks_by_datetime(query):
        if query:        
            return query.order_by(Bookmark.datetime.desc())
        else:
            return query
   
    def block(self):
        for pm in PM.from_(self.username):
            pm.db_remove()
        for pm in self.pm:
            pm.db_remove()
        for bookmark in self.bookmarks:
            bookmark.db_remove()
        for comment in Comment.get(username=self.username):
            comment.db_remove()
        for friendship in Friends.get(username1=self.username):
            friendship.db_remove()
        for friendship in Friends.get(username2=self.username):
            friendship.db_remove()        
        self.blocked = True
        return self.db_add()

orm.mapper(Account, table_account)    






################################################################################
#Permission model

table_permission = sa.schema.Table('permission', meta.metadata,
    sa.schema.Column('id', sa.types.Integer(), primary_key=True),
    sa.schema.Column('username', sa.types.Unicode(100), sa.schema.ForeignKey('account.username'), nullable=False),
    sa.schema.Column('permission', sa.types.Unicode(100), nullable=False),
    sa.schema.UniqueConstraint('username', 'permission', name='unique_userpermission'),                                
    )

class Permission(OrmObject):
    def __init__(self, username, permission):
        self.username = username
        self.permission = permission    
    
    def __repr__(self):
        return self.permission

orm.mapper(Permission, table_permission, properties={
    'accountobj': orm.relation(Account, backref='permissions') 
    })    






################################################################################
#Online model   

table_online = sa.schema.Table('online', meta.metadata,
    sa.schema.Column('id', sa.types.Integer(), primary_key=True),
    sa.schema.Column('username', sa.types.Unicode(100), nullable=False),
    sa.schema.Column('activity', sa.types.DateTime(), default=now()),
    sa.schema.UniqueConstraint('username', 'id', name='unique_activity'),
    )

class Online(OrmObject):
    def __init__(self, username):
        self.username = username

    @classmethod
    def get_or_add(cls, username):
        online = cls.get(username=username)
        return online.one() if online.count()==1 else cls(username).db_add()
    
    def update(self):
        self.activity = now()()
        return self.db_add()
        

    def set_offline(self):
        self.activity = now()()-datetime.timedelta(minutes=15)
        return self.db_add()
    
orm.mapper(Online, table_online)






################################################################################
#Friends model   

table_friends = sa.schema.Table('friend', meta.metadata,
    sa.schema.Column('id', sa.types.Integer(), primary_key=True),
    sa.schema.Column('username1', sa.types.Unicode(100), nullable=False),
    sa.schema.Column('username2', sa.types.Unicode(100), nullable=False),
    sa.schema.Column('ignore', sa.types.Boolean(), default=False),
    sa.schema.UniqueConstraint('username1', 'username2', name='unique_friendship'),                                
    )

class Friends(OrmObject):
    def __init__(self, username1, username2, ignore=False):
        self.username1 = username1
        self.username2 = username2
        self.ignore = ignore  
        
    @staticmethod
    def friends_to(username):
        for friendship in Friends.get(username1=username):
            if Friends.is_friends(friendship.username2, username):
                yield friendship.username2

    @staticmethod
    def ignored(username):
        return Friends.get(username1=username).filter(Friends.ignore==True)
                
    @staticmethod
    def is_friends(username1, username2):
        try:
            if Friends.get(username1=username1, username2=username2).one().ignore:
                return False
            if Friends.get(username1=username2, username2=username1).one().ignore:
                return False
        except:
            return False
        else:
            return True

    @staticmethod
    def is_ignore(username1, username2):
        try:
            friendship = Friends.get(username1=username1, username2=username2).one()
        except:
            return False
        else:
            return friendship.ignore
        
    @staticmethod
    def requests(username):
        #TODO cherez peresechenie zaprosov
        for friendship in Friends.get(username2=username):
            if not Friends.is_friends(friendship.username1, username) and \
               not Friends.is_ignore(username, friendship.username1) and \
               not Friends.is_ignore(friendship.username1, username):
                yield friendship.username1

orm.mapper(Friends, table_friends)






################################################################################
#Comment model

table_comment = sa.schema.Table('comment', meta.metadata,
    sa.schema.Column('id', sa.types.Integer(), primary_key=True),
    sa.schema.Column('text', sa.types.UnicodeText(), nullable=False),
    sa.schema.Column('page', sa.types.Integer(), sa.schema.ForeignKey('page.id'), nullable=False),
    sa.schema.Column('username', sa.types.Unicode(100), nullable=False),
    sa.schema.Column('spam', sa.types.Integer(), default=0),
    sa.schema.Column('datetime', sa.types.DateTime(), default=now()),
    )

class Comment(OrmObject):
    def __init__(self, username, text, page):
        if not text:
            return None
        self.username = username
        self.text = text
        self.page = page

    def db_remove(self):
        try:
            for spammark in self.spammarks:
                spammark.db_remove()
            meta.Session.delete(self)
            meta.Session.commit()
            return True
        except:
            meta.Session.rollback()
            return False
        
    @classmethod
    def remove_from_page(cls, page):
        for comment in cls.get(page = page):
            if not comment.db_remove():
                return False
        return True

    @property
    def page_title(self):
        return self.pageobj.title

    @property
    def discussion(self):
        if self.pageobj.type==u'discussion':
            return Discus.get_by_id(self.pageobj.relative)
        else:
            return None
    
    def __str__(self):
        return self.text

orm.mapper(Comment, table_comment, properties={
    'pageobj': orm.relation(Page, backref='comments'),
    })





################################################################################
#Spam model

table_spam_mark = sa.schema.Table('spam_mark', meta.metadata,
    sa.schema.Column('id', sa.types.Integer(), primary_key=True),
    sa.schema.Column('account', sa.types.Integer(), sa.schema.ForeignKey('account.id'), nullable=False),
    sa.schema.Column('comment', sa.types.Integer(), sa.schema.ForeignKey('comment.id'), nullable=False),
    sa.schema.UniqueConstraint('account', 'comment', name='unique_mark'),
    )

class SpamMark(OrmObject):
    def __init__(self, account, comment):
        self.account = account.id
        self.comment = comment.id

orm.mapper(SpamMark, table_spam_mark, properties={
    'commentobj': orm.relation(Comment, backref='spammarks') 
    })        






################################################################################
#Pm model
        
table_pm = sa.schema.Table('pm', meta.metadata,
    sa.schema.Column('id', sa.types.Integer(), primary_key=True),
    sa.schema.Column('username1', sa.types.Unicode(100), nullable=False),
    sa.schema.Column('username2', sa.types.Unicode(100), nullable=False),
    sa.schema.Column('unread', sa.types.Boolean, default=True),
    sa.schema.Column('text', sa.types.UnicodeText(), nullable=False),
    sa.schema.Column('datetime', sa.types.DateTime(), default=now()),
    sa.schema.Column('invite', sa.types.Integer(), sa.schema.ForeignKey('page.id')),
    )

class PM(OrmObject):
    def __init__(self, account1, account2, text, invitation_page=None):
        self.username1 = account1
        self.username2 = account2
        self.text = text
        self.invite = invitation_page.id if invitation_page else None
        
    @classmethod
    def to(cls, account):
        try:
            return meta.Session.query(cls).filter(cls.username2==account).order_by(cls.datetime.desc())
        except:
            return None
        
    @classmethod
    def from_(cls, account):
        try:
            return meta.Session.query(cls).filter(cls.username1==account).order_by(cls.datetime.desc())
        except:
            return None

    @classmethod
    def correspondence(cls, username1, username2):
        try:
            _from = meta.Session.query(cls).filter(cls.username1==username1).filter(cls.username2==username2)
            _to = meta.Session.query(cls).filter(cls.username1==username2).filter(cls.username2==username1)
            return _from.union(_to).order_by(cls.datetime.desc())
        except:
            return None
        
    @classmethod
    def unread_count(cls, account):
        try:
            return meta.Session.query(cls).filter(cls.username2==account).filter(cls.unread==True).count()
        except:
            return 0
        
        
    def mark_as_read(self):
        self.unread = False
        meta.Session.add(self)
        meta.Session.commit()
        return True
    
    def __repr__(self):
        return self.text

orm.mapper(PM, table_pm, properties={
    'inviteobj': orm.relation(Page, backref='invites') 
    })






################################################################################
#Bookmark model

table_bookmark = sa.schema.Table('bookmark', meta.metadata,
    sa.schema.Column('id', sa.types.Integer(), primary_key=True),
    sa.schema.Column('page', sa.types.Integer(), sa.schema.ForeignKey('page.id'), nullable=False),
    sa.schema.Column('username', sa.types.Unicode(100), nullable=False),
    sa.schema.Column('datetime', sa.types.DateTime(), default=now()),
    sa.schema.UniqueConstraint('page', 'username', name='unique_bookmark'),
    )

class Bookmark(OrmObject):
    def __init__(self, page, username):
        self.page = page
        self.username = username

    @property
    def title(self):
        return self.pageobj.title
        
    @property
    def path(self):
        return self.pageobj.path
    
    @classmethod
    def get_unique(cls, page, username):
        try:
            return meta.Session.query(cls).filter(cls.page==page).filter(cls.username==username).one()
        except:
            return None
                
    @classmethod
    def remove_for_page(cls, page):
        for bookmark in cls.get(page=page):
            if not bookmark.db_remove():
                return False
        return True
            
    
    @classmethod
    def user(cls, username):
        try:
            return meta.Session.query(cls).filter(cls.username==username)
        except:
            return None
    
    @classmethod
    def is_bookmarked(cls, page, username):
        if cls.get_unique(page, username):
            return True
        else:
            return False
        
    @classmethod
    def updates(cls, account):
        if account:
            counter = 0
            lastupdate = account.lastupdate
            for bookmark in cls.user(account.username):
                query = meta.Session.query(Comment).filter(Comment.page==bookmark.page).\
                      filter(Comment.datetime>lastupdate).filter(Comment.datetime>bookmark.datetime).filter(Comment.username!=account.username)
                for ignored_user in account.ignored:
                    query = query.filter(Comment.username!=ignored_user)
                counter+=query.count()
            return counter
        else:
            return 0

    @classmethod
    def watching(cls, page):
        return cls.get(page=page)

orm.mapper(Bookmark, table_bookmark, properties={
    'pageobj': orm.relation(Page, backref='bookmaks') 
    })





################################################################################
#News model

table_news = sa.schema.Table('news', meta.metadata,
    sa.schema.Column('id', sa.types.Integer(), primary_key=True),
    sa.schema.Column('title', sa.types.Unicode(100), nullable=False, unique=True), 
    sa.schema.Column('text', sa.types.UnicodeText()),
    sa.schema.Column('source', sa.types.Unicode(100)), 
    sa.schema.Column('datetime', sa.types.DateTime(), default=now()),
    )

class News(OrmObject):
    def __init__(self, title, text, source):
        self.title = title
        self.text = text
        self.source = source

    def render(self):
        return render_mako_def("timeline_widgets.html", "news", news=self)
    
    def render_one(self):
        return render_mako_def("timeline_widgets.html", "news_one", news=self)
    
orm.mapper(News, table_news)





################################################################################
#Playlists model

table_playlists = sa.schema.Table('playlists', meta.metadata,
    sa.schema.Column('id', sa.types.Integer(), primary_key=True),
    sa.schema.Column('title', sa.types.Unicode(100), nullable=False, unique=True),
    sa.schema.Column('username', sa.types.Unicode(100), nullable=False),
    )

table_playlist_track = sa.schema.Table('playlists_tracks', meta.metadata,
    sa.schema.Column('track', sa.types.Integer(), sa.schema.ForeignKey('track.id'), nullable=False),
    sa.schema.Column('playlist', sa.types.Integer(), sa.schema.ForeignKey('playlists.id'), nullable=False),
    )

class Playlist(OrmObject):
    def __init__(self, title, username):
        self.title = title
        self.username = username

    @classmethod
    def get_unique(cls, title, username):
        try:
            return meta.Session.query(cls).filter(cls.title==title).filter(cls.username==username).one()
        except:
            return None
    
    @classmethod
    def get_user_playlists(cls, username):
        try:
            return meta.Session.query(cls).filter(cls.username==username).group_by(cls.title).order_by(cls.title)
        except:
            return None

orm.mapper(Playlist, table_playlists, properties={
    'tracks':orm.relation(Track, secondary=table_playlist_track, backref='playlists'),
    })

