import formencode

class RegisterForm(formencode.Schema):
    allow_extra_fields = True
    filter_extra_fields = False
    username = formencode.validators.UnicodeString(not_empty=True)
    password = formencode.validators.UnicodeString(not_empty=True)
    email = formencode.validators.Email(not_empty=True)

class TrackAddForm(formencode.Schema):
    allow_extra_fields = True
    filter_extra_fields = True
    title = formencode.validators.UnicodeString(not_empty=True)
    artist = formencode.validators.UnicodeString(not_empty=True)
    file = formencode.validators.FieldStorageUploadConverter(not_empty=True)
    
class TrackEditForm(formencode.Schema):
    allow_extra_fields = True
    filter_extra_fields = True
    title = formencode.validators.UnicodeString(not_empty=True)
    artist = formencode.validators.UnicodeString(not_empty=True)
    file = formencode.validators.FieldStorageUploadConverter()    
    
class ArtistForm(formencode.Schema):
    allow_extra_fields = True
    filter_extra_fields = True
    id = formencode.validators.Int(not_empty=True)
    name = formencode.validators.UnicodeString(not_empty=True)
    who = formencode.validators.UnicodeString()
    info = formencode.validators.UnicodeString()

class DiscussionForm(formencode.Schema):
    allow_extra_fields = True
    filter_extra_fields = True
    title = formencode.validators.UnicodeString(not_empty=True)
    theme = formencode.validators.UnicodeString(not_empty=True)
    
class ResetPassword(formencode.Schema):
    allow_extra_fields = True
    filter_extra_fields = True
    old = formencode.validators.UnicodeString(not_empty=True)
    new = formencode.validators.UnicodeString(not_empty=True)

class NewsForm(formencode.Schema):
    allow_extra_fields = True
    filter_extra_fields = True
    title = formencode.validators.UnicodeString(not_empty=True)
    text = formencode.validators.UnicodeString()
    source = formencode.validators.UnicodeString()
    
