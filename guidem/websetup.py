"""Setup the guidem application"""
import os
import logging

from guidem.config.environment import load_environment
from guidem.model import meta
import guidem.model
from guidem.lib.auth import Users as users


from sync import gen

log = logging.getLogger(__name__)

def setup_app(command, conf, vars):
    """Place any commands to setup guidem here"""
    load_environment(conf.global_conf, conf.local_conf)

    #Clear
    #meta.metadata.drop_all(bind=meta.engine)
    meta.metadata.create_all(bind=meta.engine)
    meta.Session.commit()    
        
    gen.generate('./guidem/public/music')
    
    users.user_create(u'admin', u'123', u'andrew@ya.ru', [u'admin'])
    users.user_create(u'andrew', u'123', u'non@non.ru', [u'admin'])
    users.user_create(u'qwe', u'12', u'gick@hon.ru')

    filename = os.path.split(conf.filename)[-1]
    if filename == 'test.ini':
        #Fixture
        guidem.model.News(u'Test news', u'text of test news', u'guidem.ru').db_add()
        guidem.model.Discus(u'test discussion', u'test discussion theme').db_add()
        guidem.model.Discus(u'test discussion 2', u'test discussion theme 2').db_add()
        guidem.model.Page(u'news', 1).db_add()
        guidem.model.Page(u'discus', 1).db_add()
        guidem.model.Page(u'discus', 2).db_add()
        guidem.model.Bookmark(2, u'admin').db_add()
        guidem.model.Bookmark(3, u'admin').db_add()
        guidem.model.Comment(u'admin', u'hola', 2).db_add()
        guidem.model.Comment(u'admin', u'holol', 3).db_add()
        guidem.model.Comment(u'admin', u'holo1l', 3).db_add()
        guidem.model.Playlist(u'test playlist', u'admin').db_add()
        guidem.model.Playlist(u'page playlist', u'admin').db_add()
        f = guidem.model.File('1.jpg', './guidem/public/music/upload/9a7b54711a3875326d382e082d739886.jpg', 1024).db_add()
        guidem.model.Image(u'Test image', u'Test desc', f).db_add()
        guidem.model.Image(u'Test image', u'Test desc', f).db_add()
        guidem.model.Friends(u'admin', u'qwe').db_add()
    
