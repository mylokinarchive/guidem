from guidem.tests import *

class TestPageController(TestController):
    def test_song(self):
        response = self.app.get(
            url(controller='page', action='song', id=1),
            status = 200
        )
        
    def test_artist(self):
        response = self.app.get(
            url(controller='page', action='artist', id=1),
            status = 200
        )

    def test_discussion(self):
        response = self.app.get(
            url(controller='page', action='discussion', id=2),
            status = 200
        )
        
    def test_news(self):
        response = self.app.get(
            url(controller='page', action='news', id=1),
            status = 200
        )

    def test_search(self):
        response = self.app.get(
            url(controller='page', action='search'),
            params = {'query':u'a'},
            status = 200
        )
        
    def test_playlist(self):
        response = self.app.get(
            url(controller='page', action='playlist', username=u'admin', title=u'page playlist'),
            status = 200
        )        