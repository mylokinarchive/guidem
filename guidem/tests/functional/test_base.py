from guidem.tests import *

class TestBaseController(TestController):
    #def test_home(self):
        #response = self.app.get(
            #url(controller='base', action='home'),
            #status = 200
        #)

    def test_tracks(self):
        response = self.app.get(
            url(controller='base', action='tracks'),
            params = {'items_per_page':1},
            status = 200
        )
        
    def test_tracks_letter(self):
        response = self.app.get(
            url(controller='base', action='tracks_letter', letter='A'),
            params = {'items_per_page':1},
            status = 200
        )

    def test_artists(self):
        response = self.app.get(
            url(controller='base', action='artists'),
            params = {'items_per_page':1},
            status = 200
        )
        
    def test_artists_letter(self):
        response = self.app.get(
            url(controller='base', action='artists_letter', letter='A'),
            params = {'items_per_page':1},
            status = 200
        )

    def test_discussion(self):
        response = self.app.get(
            url(controller='base', action='discussions'),
            params = {'items_per_page':1},
            status = 200
        )        
        
    def test_add_discussions(self):
        response = self.app.post(
            url(controller='base', action='add_discussion'),
            params = {'title':u'Test discussion', 'theme':u'Testing...'},
            extra_environ={'TESTING':'test'},
            status = 302
        )
        assert response.session['flash']        

    def test_edit_discus(self):
        response = self.app.get(
            url(controller='admin', action='edit_discus'),
            params = {'id':1},
            extra_environ={'TESTING':'test'},
            status = 200
        )
    
    def test_edit_discus_submit(self):
        response = self.app.post(
            url(controller='admin', action='edit_discus_submit'),
            params = {'id':1,
                      'title':u'Title ed',
                      'theme':u'Theme ed'},
            extra_environ={'TESTING':'test'},
            status = 302
        )
        assert response.session['flash']        
        
    def test_remove_discus(self):
        response = self.app.post(
            url(controller='admin', action='remove_discus'),
            params = {'id':u'1'},
            extra_environ={'TESTING':'test'},
            status = 302
        )
        assert response.session['flash']        
        
    def test_timeline(self):
        response = self.app.get(
            url(controller='base', action='timeline'),
            params = {'items_per_page':1},
            status = 200
        )
    
    def test_feed(self):
        response = self.app.get(
            url(controller='base', action='feed'),
            params = {'items_per_page':1},
            status = 200
        )

        