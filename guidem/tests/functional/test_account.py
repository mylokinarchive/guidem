# -*- coding: utf-8 -*-
from guidem.tests import *

class TestAccountController(TestController):
    def test_account(self):
        response = self.app.get(
            url(controller='account', action='account'),
            extra_environ={'TESTING':'test'},
            status = 200
        )
        
    def test_bookmarks(self):
        response = self.app.get(
            url(controller='account', action='bookmarks'),
            extra_environ={'TESTING':'test'},
            status = 200
        )

    def test_friends(self):
        response = self.app.get(
            url(controller='account', action='friends'),
            extra_environ={'TESTING':'test'},
            status = 200
        )
        
    def test_friends_timeline(self):
        response = self.app.get(
            url(controller='account', action='friends_timeline'),
            extra_environ={'TESTING':'test'},
            status = 200
        )

    def test_ignored(self):
        response = self.app.get(
            url(controller='account', action='ignored'),
            extra_environ={'TESTING':'test'},
            status = 200
        )
        
    def test_pm(self):
        response = self.app.get(
            url(controller='account', action='pm'),
            extra_environ={'TESTING':'test'},
            status = 200
        )
        
    def test_read_updates(self):
        response = self.app.get(
            url(controller='account', action='read_updates'),
            extra_environ={'TESTING':'test'},
            status = 302
        )
        assert response.environ['_auth']

    def test_register(self):
        response = self.app.get(
            url(controller='account', action='register'),
        )        

    def test_requests(self):
        response = self.app.get(
            url(controller='account', action='requests'),
            extra_environ={'TESTING':'test'},
            status = 200
        )
        
    def test_reset_password(self):
        response = self.app.get(
            url(controller='account', action='reset_password'),
            extra_environ={'TESTING':'test'},
            status = 200
        )
        
    def test_signin(self):
        response = self.app.get(
            url(controller='account', action='signin'),
        )
        
    def test_updates(self):
        response = self.app.get(
            url(controller='account', action='updates'),
            extra_environ={'TESTING':'test'},
            status = 200
        )
        
    def test_read_bookmark(self):
        response = self.app.get(
            url(controller='account', action='read_bookmark'),
            params = {'page':u'2' },
            extra_environ={'TESTING':'test'},
            status = 302
        )
        assert response.environ['_auth']
    
    def test_requests(self):
        response = self.app.get(
            url(controller='account', action='requests'),
            extra_environ={'TESTING':'test'},
            status = 200
        )
        
    def test_playlists(self):
        response = self.app.get(
            url(controller='account', action='playlists'),
            extra_environ={'TESTING':'test'},
            status = 200
        )

    def test_add_to_playlist(self):
        response = self.app.get(
            url(controller='account', action='add_to_playlist'),
            params = {'title':u'test playlist', 'track':2},
            extra_environ={'TESTING':'test'},
            status = 302
        )     
        assert response.session['flash']
        
    def test_remove_from_playlist(self):
        response = self.app.get(
            url(controller='account', action='remove_from_playlist'),
            params = {'title':u'test playlist', 'track':2},
            extra_environ={'TESTING':'test'},
            status = 302
        )     
        assert response.session['flash']

        
    def test_create_playlist(self):
        response = self.app.get(
            url(controller='account', action='create_playlist'),
            params = {'track':3},
            extra_environ={'TESTING':'test'},
            status = 200
        )

    def test_create_playlist_submit(self):
        response = self.app.get(
            url(controller='account', action='create_playlist_submit'),
            params = {'title':u'test new playlist', 'track':3},
            extra_environ={'TESTING':'test'},
            status = 302
        )     
        assert response.session['flash']

    def test_remove_playlist(self):
        response = self.app.get(
            url(controller='account', action='remove_playlist'),
            params = {'title':u'test new playlist'},
            extra_environ={'TESTING':'test'},
            status = 302
        )
        assert response.session['flash']
        
    def test_account_info(self):
        response = self.app.get(
            url(controller='account', action='info'),
            extra_environ={'TESTING':'test'},
            status = 200
        )

    def test_account_info_submit(self):
        response = self.app.get(
            url(controller='account', action='info_submit'),
            params = {'info':u'Info test'},
            extra_environ={'TESTING':'test'},
            status = 302
        )
        assert response.session['flash']

    def test_correspondence(self):
        response = self.app.get(
            url(controller='account', action='correspondence', username='andrew'),
            extra_environ={'TESTING':'test'},
            status = 200
        )
        
    def test_show_account(self):
        response = self.app.get(
            url(controller='account', action='show', username='andrew'),
            extra_environ={'TESTING':'test'},
            status = 200
        )
    
    def test_reset_password_submit(self):
        response = self.app.post(
            url(controller='account', action='reset_password_submit'),
            params = {'old':u'123', 'new':u'12'},
            extra_environ={'TESTING':'test'},
            status = 302
        )
        assert response.session['flash']
        
    def test_register_submit(self):
        response = self.app.post(
            url(controller='account', action='submit'),
            params = {'username':u'test', 'password':u'12'},
            status = 302
            )
        assert not response.session.has_key('flash-error')
        
    def test_signin_submit(self):
        response = self.app.post(
            url(controller='account', action='signin_submit'),
            params = {'username':u'andrew', 'password':u'123'},
            status = 302
            )
        assert not response.session.has_key('flash-error')

    def test_signout(self):
        response = self.app.post(
            url(controller='account', action='signout'),
            extra_environ={'TESTING':'test'},
            status = 302
            )
        assert response.headers['Set-Cookie']
        