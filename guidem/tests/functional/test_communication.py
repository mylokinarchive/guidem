from guidem.tests import *

class TestCommunicationController(TestController):
    def test_add_bookmark(self):
        response = self.app.get(
            url(controller='communication', action='add_bookmark'),
            params = {'page':u'1'},            
            extra_environ={'TESTING':'test'},
            status = 302
        )
        assert response.session['flash']
        

    def test_remove_bookmark(self):
        response = self.app.get(
            url(controller='communication', action='remove_bookmark'),
            params = {'page':u'1'}, 
            extra_environ={'TESTING':'test'},
            status = 302
        )
        assert response.session['flash']
        
    def test_pm(self):
        response = self.app.get(
            url('pm'),
            params = {
                'account':u'andrew',
                'text':u'test text'
                },
            extra_environ = {'TESTING':'test'},
            status = 302
        )        
        assert response.session['flash']

    def test_invite(self):
        response = self.app.get(
            url('invite'),
            params = {
                'page':'1'
                },
            extra_environ = {'TESTING':'test'},
            status = 200
        )
        response = self.app.get(
            url('invite'),
            params = {
                'page':'/discus/1'
                },
            extra_environ = {'TESTING':'test'},
            status = 404
        )    
        
    def test_invite_submit(self):
        response = self.app.get(
            url('invite_submit'),
            params = {
                'page':'1',
                'text':'test invite',
                'username1':u'admin',
                'username2':u'qwe',
                },
            extra_environ = {'TESTING':'test'},
            status=302
        )
        assert response.session['flash']

    def test_add_comment(self):
        response = self.app.get(
            url('add_comment'),
            headers = {'Referer':'/discus/3'},
            params = {
                'text':u'Comment invite',
                },
            extra_environ = {'TESTING':'test'},
            status=302
        )
        assert response.session['flash']
        
    def test_remove_myself_comment(self):
        response = self.app.get(
            url('remove_myself_comment'),
            headers = {'Referer':'/discus/3'},
            params = {'comment':3},
            extra_environ = {'TESTING':'test'},
            status=302
        )
        assert response.session['flash']
        
    def test_add_friend(self):
        response = self.app.get(
            url('add_friend'),
            params = {'id':u'andrew'},
            extra_environ = {'TESTING':'test'},
            status = 302
        )        
        assert response.session['flash']  

    def test_remove_friend(self):
        response = self.app.get(
            url('remove_friend'),
            params = {'id':u'andrew'},
            extra_environ = {'TESTING':'test'},
            status = 302
        )        
        assert response.session['flash']        
        
    def test_ignore(self):
        response = self.app.get(
            url('ignore'),
            params = {'id':u'qwe'},
            extra_environ = {'TESTING':'test'},
            status = 302
        )        
        assert response.session['flash']
        
    def test_mark_as_spam(self):
        response = self.app.get(
            url('mark_as_spam'),
            params = {'id':3},
            extra_environ = {'TESTING':'test'},
            status = 302
        )        
        assert response.session['flash']
