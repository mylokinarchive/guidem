from guidem.tests import *

class TestAdminController(TestController):
    def test_home(self):
        response = self.app.get(
            url(controller='admin', action='home'),
            extra_environ={'TESTING':'test'},
            status = 200            
        )
    
    def test_images(self):
        response = self.app.get(
            url(controller='admin', action='images'),
            extra_environ={'TESTING':'test'},
            status = 200
        )

    def test_stream(self):
        response = self.app.get(
            url(controller='admin', action='stream'),
            extra_environ={'TESTING':'test'},
            status = 200
        )

    def test_master_track(self):
        response = self.app.get(
            url(controller='admin', action='master_track'),
            extra_environ={'TESTING':'test'},
            status = 200
        )
    
    def test_add_track_submit(self):
        response = self.app.post(
            url(controller='admin', action='master_track_submit'),
            params = {'title':u'Track', 'artist':u'Artist'},
            upload_files = [('file', '1.mp3', open('/home/andrew/tmp/track3.mp3', 'rb').read(1024))],
            extra_environ={'TESTING':'test'},
            status = 302
        )
        assert not response.session.has_key('flash-error')

         
        
    def test_add_image(self):
        response = self.app.get(
            url(controller='admin', action='add_image'),
            extra_environ={'TESTING':'test'},
            status = 200
        )
    
    def test_add_image_submit(self):
        response = self.app.post(
            url(controller='admin', action='add_image_submit'),
            params = {'title':u'Track', 'description':u'Description'},
            upload_files = [('file', '1.jpg', 'THIS IfasdfS JPG!!!!!111')],
            extra_environ={'TESTING':'test'},
            status = 302
        )
        assert not response.session.has_key('flash-error')

    def test_add_news(self):
        response = self.app.get(
            url(controller='admin', action='add_news'),
            extra_environ={'TESTING':'test'},
            status = 200
        )
    
    def test_add_news_submit(self):
        response = self.app.post(
            url(controller='admin', action='add_news_submit'),
            params = {'title':u'News', 'text':u'Text', 'source':u'guidem.ru'},
            extra_environ={'TESTING':'test'},
            status = 302
        )
        assert response.session['flash']

    def test_edit_artist(self):
        response = self.app.get(
            url(controller='admin', action='edit_artist'),
            params = {'id':1},
            extra_environ={'TESTING':'test'},
            status = 200
        )
    
    def test_edit_artist_submit(self):
        response = self.app.post(
            url(controller='admin', action='edit_artist_submit'),
            params = {'id':1,
                      'name':u'Artist ed',
                      'who':u'who ed',
                      'info':u'info ed'},
            extra_environ={'TESTING':'test'},
            status = 302
        )
        assert response.session['flash']

    def test_edit_news(self):
        response = self.app.get(
            url(controller='admin', action='edit_news'),
            params = {'id':1},
            extra_environ={'TESTING':'test'},
            status = 200
        )
    
    def test_edit_news_submit(self):
        response = self.app.post(
            url(controller='admin', action='edit_news_submit'),
            params = {'id':'1',
                      'title':u'Title ed',
                      'text':u'Text ed',
                      'source':u'guidem.ru'},
            extra_environ={'TESTING':'test'},
            status = 302
        )
        assert response.session['flash']
        
    def test_edit_track(self):
        response = self.app.get(
            url(controller='admin', action='edit_track'),
            params = {'id':1},
            extra_environ={'TESTING':'test'},
            status = 200
        )
    
    def test_edit_track_submit(self):
        response = self.app.post(
            url(controller='admin', action='edit_track_submit'),
            params = {'id':1,
                      'title':u'Title ed',
                      'artist':u'Text ed'},
            upload_files = [('file', '1ed.mp3', open('/home/andrew/tmp/track3.mp3', 'rb').read())],
            extra_environ={'TESTING':'test'}, 
            status = 302
            
        )
        assert response.session['flash']
        
    
    #def test_remove_song(self):
        #response = self.app.post(
            #url(controller='admin', action='remove_song'),
            #params = {'id':1},
            #extra_environ={'TESTING':'test'},
            #status = 302
            
        #)
        #assert response.session['flash']
    
    #def test_remove_image(self):
        #response = self.app.post(
            #url(controller='admin', action='remove_image'),
            #params = {'id':2},
            #extra_environ={'TESTING':'test'},
            #status = 302
        #)
        #assert response.session['flash']
    
    #def test_remove_artist(self):
        #response = self.app.post(
            #url(controller='admin', action='remove_artist'),
            #params = {'id':1},
            #extra_environ={'TESTING':'test'}
        #)
        #assert response.session['flash']
        
    def test_set_admin_role(self):
        response = self.app.post(
            url(controller='admin', action='set_admin_role'),
            params = {'username':u'andrew'},
            extra_environ={'TESTING':'test'},
            status = 302
        )
        assert response.session['flash']

    def test_block(self):
        response = self.app.post(
            url(controller='admin', action='block'),
            params = {'id':2},
            extra_environ={'TESTING':'test'},
            status = 302
        )
        assert response.session['flash']

    def test_unblock(self):
        response = self.app.post(
            url(controller='admin', action='unblock'),
            params = {'id':2},
            extra_environ={'TESTING':'test'},
            status = 302
        )
        assert response.session['flash']        
    
    def test_remove_comment(self):
        response = self.app.post(
            url(controller='admin', action='remove_comment'),
            params = {'comment':2},
            extra_environ={'TESTING':'test'},
            status = 302
        )
        assert response.session['flash']
        
    def test_confirm_track(self):
        response = self.app.post(
            url(controller='admin', action='confirm_track'),
            params = {'id':1},
            extra_environ={'TESTING':'test'},
            status = 302
        )
        assert response.session['flash']
