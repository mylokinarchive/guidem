var guidemAction = null;
var guidemWidget = null;
var guidemPlayer = null;

//widgets
function Play(){
    var state = NaN;
    var self = this;
    
    this.init = function(){
        this.state = "play";
        this.dom = $("#player-play");    
        this.render_play_state();
    };
    
    this.render_play_state = function(){
        self.state = "play";    
        this.dom.button({
            text: false,
            icons: {primary: "ui-icon-play"}
        });
    };

    this.render_pause_state = function(){
        self.state = "pause";
        this.dom.button({
            text: false,
            icons: {primary: "ui-icon-pause"}
        });
    };
        
    this.click = function(event){
        if(self.state=="play"){            
            self.render_pause_state();
        } else {
            self.render_play_state();
        }
        return self.state;
    };
}

function RV(){
    this.init = function(){
        this.dom = $("#player-rv");        
        this.render();
    };
    
    this.render = function(){
        this.dom.button({
            text: false,
            icons: {primary: "ui-icon-seek-start"}
        });
    };
}

function FF(){
    this.init = function(){
        this.dom = $("#player-ff");        
        this.render();
    };
    
    this.render = function(){
        this.dom.button({
            text: false,
            icons: {primary: "ui-icon-seek-end"}
        });        
    };   
}

function Progressbar(){
    var self = this;

    this.init = function(){
        this.dom = $("#player-progressbar");
        this.render();
    };

    this.position = function(percents){
        this.dom.progressbar("option", "value", percents);    
    };
    
    this.click = function(event){
        element_width = self.dom.width();
        element_position = self.dom.position().left;
        mouse_position = event.pageX;
        percents = Math.round((mouse_position-element_position)/element_width*100);
        self.position("option", "value", percents);
        return percents;
    };
    
    this.render = function(){
        this.dom.progressbar();    
    };
}

function Loading(){
    this.init = function(){
        this.dom = $("#player-loading");
        this.render();
    };
    
    this.hide = function(){
        this.dom.slideUp();
    };
    
    this.show = function(){
        this.dom.slideDown();
    };
    
    this.render = function(){
        this.hide();
    };
    
}

function Duration(){
    var self = this;
    
    this.init = function(){
        this.dom = $("#player-durationbar");
        this.duration = $("#player-duration");
        this.durationestimates = $("#player-durationestimates");
        this.render();
    };
    
    this.hide = function(){
        this.dom.hide();
    };
    
    this.show = function(){
        this.dom.show();
    };
    
    this.render = function(){
        this.hide();
    };
    
    this.set_duration = function(milliseconds){
        text = this.calculate(milliseconds);
        this.duration.text(text);
    };
    
    this.set_durationestimates = function(milliseconds){
        text = this.calculate(milliseconds);
        this.durationestimates.text(text);
    };

    this.clear = function(){
        this.set_duration(0);
        this.set_durationestimates(0);
    };
    
    this.calculate = function(milliseconds){
        minutes = Math.floor(milliseconds/1000/60);
        seconds = Math.floor(milliseconds/1000)-minutes*60;
        return minutes+":"+seconds;
    };
}

function RadioControll(){
    var self = this;
    
    this.init = function(){
        this.on = $("#player-radio-on");
        this.off = $("#player-radio-off");
        this.render();
    };
        
    this.render = function(){
        this.on.button({
            text: false,
            icons: {primary: 'ui-icon-volume-on'}
        }).next().button({
            text: false,
            icons: {primary: 'ui-icon-volume-off'}
        }).parent().buttonset();            
    };
}


function Info(){
    this.init = function(){
        this.dom = $("#player-info");
        this.render();
    };
    
    this.hide = function(){
        this.dom.hide();
    };
    
    this.show = function(){
        this.dom.show();
    };
    
    this.render = function(){
        this.hide();
    };
    
    this.text = function(text){
        this.dom.text(text);
        this.show();
    };
    
}


function Playlist(){
    var self = this;
    
    this.init = function(){
        this.dom = $("#player-playlist");
    };

    this.repeat = function(){
        return $('#player-playlist-repeat').attr('checked');
    };
    
    this.shuffle = function(){
        return $('#player-playlist-shuffle').attr('checked');
    };
    
    this.add = function(id){
        $.get('/player/track/'+id+'/render', function(data){
            self.dom.append(data);
        });                    
    };
    
}

function History(){
    var self = this;
    
    this.init = function(){
        this.dom = $("#player-history");
    };
    
    this.add = function(id){
        $.get('/player/track/'+id+'/render_history', {counter:this.counter}, function(data){
            self.dom.prepend(data);
            while(self.dom.children().length>10){
                self.dom.children().last().remove();
            }
        });                    
    };
}

function Radio(){
    var self = this;
    
    this.state = false;
    
    this.init = function(){
        this.dom = $("#player-radio");
    };
    
    this.add = function(id){
        $.get('/player/track/'+id+'/render', function(data){
            self.dom.prepend(data);
            widget.playnow(self.dom.children().children().first());
            while(self.dom.children().length>10){
                self.dom.children().last().remove();
            }
        });                    
    };
}

function Widgets(){
    this.play = new Play();
    this.rv = new RV();
    this.ff = new FF();
    this.progressbar = new Progressbar();
    this.loading = new Loading();
    this.duration = new Duration();
    this.radio = new RadioControll();
    this.info = new Info();
    
    this.render = function(){
        this.loading.init();    
        this.play.init();
        this.rv.init();
        this.ff.init();
        this.progressbar.init();
        this.duration.init();
        this.radio.init();
        this.info.init();
        
        $("#player-tabs").tabs();
    };
    
    this.playnow = function(element){
        while($('.playnow').length){
            $('.playnow').first().removeClass('playnow');
        }
        element.addClass('playnow');
    };

    this.enable = function (){
        this.progressbar.dom.progressbar("enable");
        this.play.dom.button("enable");
        this.rv.dom.button("enable");
        this.ff.dom.button("enable");
    }; 

    this.disable = function (){
        this.progressbar.dom.progressbar("disable");
        this.play.dom.button("disable");
        this.rv.dom.button("disable");
        this.ff.dom.button("disable");
    };	    

    
}

function Player(){
    var self = this;

    this.playlist = new Playlist();
    this.history = new History();
    this.radio = new Radio();
    
    this.init = function(){
        this.playlist.init();
        this.history.init();
        this.radio.init();
        widget.render();
        widget.disable();
        
        //reactions
        widget.play.dom.click(function(event){
            state = widget.play.click(event);
            if(state=="play"){
                self.play.current.pause();
            } else {
                self.play.current.play();
            }
        });
        
        widget.progressbar.dom.click(function(event){
            percents = widget.progressbar.click(event);
            position = Math.round((self.play.current.durationEstimate/100)*percents);
            self.play.current.setPosition(position);
            self.event.lastupdate = 0;
        });
        
        widget.ff.dom.click(self.ff);
        
        widget.rv.dom.click(function(){
            if(self.history.dom.find('.playnow').length){
                self.history.dom.find('.playnow').parent().next().children().click();
            }else{
                self.history.dom.children().first().children().click();
            }
        });
        
        widget.radio.on.click(function(){
            self.radio.state = true;
            $.getJSON('/radio', function(data){
                action.play(data.id);
                self.radio.add(data.id);
            });
        });
        
        widget.radio.off.click(function(){
            self.radio.state = false;
            self.stop();
        });
    };
    
    this.play = {
        current: false,
        load: function(url){
            self.event.lastupdate = 0;
            self.event.loadingupdate = 0;
            if(this.current!==false){
                this.current.destruct();
            }
            this.current = sm.createSound({
                id: 'current',
                url: url,
                whileplaying: self.event.whileplaying,
                whileloading: self.event.whileloading,
                onload: self.event.onload,
                onfinish: self.event.onfinish 
            });
        },
        url: function(url){
            self.play.load(url);
            this.current.play();
        },
        id: function(id){
            self.history.add(id);
            $.getJSON('/player/track/'+id, function(data){
                self.play.url(data.url);
                widget.info.text(data.artist+' - '+data.track);
            });
        },
        back: function(id){
            $.getJSON('/player/track/'+id, function(data){
                self.play.url(data.url);
                widget.info.text(data.artist+' - '+data.track);
            });
        },
        load_id: function(id){
           self.history.add(id);
            $.getJSON('/player/track/'+id, function(data){
                self.play.load(data.url);
                widget.info.text(data.artist+' - '+data.track);
            });
        }
    }
    
    this.event = {
        lastupdate: 0,
        loadingupdate: 0, 
        whileplaying: function(){
            if (self.event.lastupdate+500<this.position){
                percents = this.position/this.durationEstimate*100;
                widget.progressbar.position(percents);
                widget.duration.set_durationestimates(self.play.current.position);
                self.event.lastupdate = this.position;
            }
        },
        whileloading: function(){
            if (self.event.loadingupdate+1000<this.bytesLoaded){
                widget.loading.show();
                widget.duration.set_duration(self.play.current.durationEstimate);
                self.event.loadingupdate = this.bytesLoaded;
            }
        },
        onload: function(){
            widget.duration.set_duration(self.play.current.durationEstimate);
            widget.loading.hide();
        },
        onfinish: function(){
            self.ff()
        }
    }
    
    this.pause = function(){
        this.play.current.pause();
    }
    
    this.stop = function(){
        this.play.current.stop();
        this.event.lastupdate = 0;
        widget.play.render_play_state();
        widget.progressbar.position(0);    
    }
    
    this.ff = function(){
        element = $('.playnow').first().parent().next();
        if(self.radio.state){
            action.radio_on();
        } else {
            if(self.playlist.shuffle()){
                //TODO
                length = $('#player-playlist').children().length;
                step = 1/length;
                index = 0;
                random = Math.random()
                while((step*index)<random)
                    index++;
                index--;
                element = $('#player-playlist').children()
                element.eq(index).children().first().click();
            } else if(element.length){
                element = element.first();
                element.children().first().click();
            } else if(self.playlist.repeat()){
                $('#player-playlist').children().first().children().first().click();
            }
        }
    }
}

function Action(){
    this.play = function(id) {
        widget.enable();
        widget.loading.show();
        widget.play.render_pause_state();
        widget.duration.clear();
        widget.duration.show();
        widget.progressbar.position(0);
        player.play.id(id);
    }
    this.load = function(id) {
        widget.enable();
        widget.play.render_play_state();
        widget.duration.clear();
        widget.duration.show();
        widget.progressbar.position(0);
        player.play.load_id(id);
    }
    this.back = function(id) {
        widget.play.render_pause_state();
        widget.duration.clear();
        widget.duration.show();
        widget.progressbar.position(0);
        player.play.back(id);
    }
    this.add = function(id) {
        widget.enable();
        player.playlist.add(id);
        player.playlist.dom.children().last().children().first().click();
    }
    this.radio_on = function(){
        widget.enable();
        $('#player-tabs').tabs('select', 2);
        widget.radio.on.click();
    }
    this.radio_off = function(){
        widget.radio.off.click();
    }
    
}

var action = new Action();
var widget = new Widgets();
var player = new Player();