function bindEvents(){
    window.widget = {
	play: $("#playImg"),
	ff: $("#ffImg"),
	title: $("#descriptionTitle"),
	artist: $("#descriptionArtist"),
	volume: $("#volumeImg"),
	pVolume: $("#volumeBar"),
	pVolumeCtrl: $("#volumeBarCtrl"),
	pPlay: $("#pgbPlay"), 
	pLoad: $("#pgbLoading"),
	pRew: $("#pgbRewinding"),
	cPlay: $("#playCounter"),
	cTotal: $("#totalCounter"),
	historyPlay: $(".trackPlayImg"),
    };

    widget.play.click(function(){
	if (window.api.radio.currentSound) {
	    window.api.radio.currentSound.togglePause();
	} else {
	    window.api.playRandomTrack();
	}
    });

    widget.ff.click(function(){
	window.api.playRandomTrack();
    });
    
    widget.pRew.click(function(event){
	if(api.radio.currentSound){
	    percents = pgb.click(event);
	    position = Math.round((api.radio.currentSound.durationEstimate/100)*percents);
	    api.radio.currentSound.setPosition(position);
	}
    });
    
    widget.pVolumeCtrl.mousemove(function(event){
	if(window.vol_slide&&!window.vol_click){
	    percents = volume.click(event);
	    volume.render(percents);
	    if(api.radio.currentSound){
		volume.set(api.radio.currentSound, percents);
	    };
	}
    });
    widget.pVolumeCtrl.mouseenter(function(){
	window.vol_slide = false;
	window.vol_click = false;
    });
    widget.pVolumeCtrl.mouseout(function(){
	window.vol_slide = false;
	window.vol_click = false;
    });
    
    widget.pVolumeCtrl.mousedown(function(){
	window.vol_slide = false;
	window.vol_click = false;
	setTimeout(function(){
	    if(!window.vol_click){
		window.vol_slide = true;
	    }
	}, 100);
    });
    widget.pVolumeCtrl.click(function(event){
	if(!window.vol_slide)
	    window.vol_click = true;
	else
	    window.vol_slide = false;
	percents = volume.click(event);
	volume.render(percents, true);
	if(api.radio.currentSound){
	    volume.set(api.radio.currentSound, percents);
	};
    });	

    widget.volume.click(function(){
	if(api.radio.currentSound){
	    volume.toggleMute(api.radio.currentSound);
	};
    });

    historyWidget.refresh();
    
};

function ProgressBar() {
    this.setPlay = function(percents){
	widget.pPlay.animate({"width":percents+"%"}, 'fast');
    };
    this.setPlayCounter = function(time){
	widget.cPlay.text(time);
    };
    this.setLoad = function(percents){
	widget.pLoad.animate({"width":percents+"%"}, 'fast');
    };
    this.setTotalCounter = function(time){
	widget.cTotal.text(time);
    };
    this.click = function(event){
	var e = widget.pRew;
        var element_width = e.width();
        var element_position = e.parent().position().left;//adopt to position:absolute;
        var mouse_position = event.pageX;
        var percents = Math.round((mouse_position-element_position)/element_width*100);
        return percents;
    };
        
}

pgb = new ProgressBar();

function VolumeBar() {
    this.volume = 75;
    this.muted = false;
    this.click = function(event){
	var e = widget.pVolumeCtrl;
	var element_height = e.height();
	var element_position = e.parent().position().top + element_height;
	var mouse_position = event.pageY;
	var percents = Math.round((element_position - mouse_position)/element_height*100);
	return percents;
    };
    this.render = function(percents, animate){
	if(animate)
	    widget.pVolume.animate({"height":percents+"%"});
	else
	    widget.pVolume.css({"height":percents+"%"});	    
	if(this.muted){
	    widget.volume.attr("src", "/images/volume_mute.png");
	}else{
	    widget.volume.attr("src", "/images/volume.png");
	};
	
    };
    this.set = function(soundObj, percents, render, animate){
	soundObj.setVolume(percents);
	soundObj.unmute();
	this.volume = percents;
	this.muted = false;
	if(render)
	    this.render(percents, animate);
    };
    this.conf = function(soundObj){
	this.render(this.volume);
    };
    this.toggleMute = function(soundObj){
	soundObj.toggleMute();
	this.muted = soundObj.muted;
	this.render();
    };
};

volume = new VolumeBar();

historyWidget = {
    add: function(id){
	track = api.radio.soundData[id];
	element = $('.trackBoxHidden').first().clone();
	element.children('.trackBoxArtist').text(track.artist);
	element.children('.trackBoxArtist').attr('href', track.artist_url);
	element.children('.trackBoxTitle').text(track.title);
	element.children('.trackBoxTitle').attr('href', track.track_url);
	element.children('.trackBoxId').text(id);
	element.removeClass('.trackBoxHidden');
	element.insertAfter($('.trackBoxHidden').first());
    	element.fadeIn(300);
	while($('.trackBox').length>50){
	    var lastElement = $('.trackBox').last();
	    sm.destroySound(lastElement.children('.trackBoxId').text());
	    lastElement.remove();
	};
	$('.lastTrackBox').removeClass('lastTrackBox');
	$('.trackBox').last().addClass('lastTrackBox');
	this.refresh();
    },
    refresh: function(){
	widget.historyPlay = $(".trackPlayImg");	
	widget.historyPlay.unbind('click');
        widget.historyPlay.bind('click', function(){
	    api.radio.playSound($(this).next('.trackBoxId').text());
	});
    },
}

function Radio() {
    var self = this;

    this.currentSound = NaN;
    this.soundData = {};

    this.calculate = function(milliseconds){
        function padDigits(n, totalDigits){ 
            n = n.toString(); 
            var pd = ''; 
            if (totalDigits > n.length){ 
                for (i=0; i < (totalDigits-n.length); i++){ 
		    pd += '0'; 
		};
            } 
            return pd + n.toString(); 
        };
        minutes = Math.floor(milliseconds/1000/60);
        seconds = Math.floor(milliseconds/1000)-minutes*60;
        return minutes+":"+padDigits(seconds, 2);
    };    

    this.createSound = function(id, url){
	var existTrack = sm.getSoundById(id);
	if (existTrack){
	    return existTrack;
	}
	if (sm.canPlayURL(url)){
	    var track = sm.createSound({
		id: id,
		url:url,
		onplay: self.event.onplay,
		onfinish: self.event.onfinish,
		whileplaying: self.event.whileplaying,
		whileloading: self.event.whileloading,
		onload: self.event.onload,
		onpause: self.event.onpause,
		onresume: self.event.onresume,
		volume: volume.volume,
		muted: volume.muted,
	    });
	    return track;
	} else {
	    message('you fucked');
	    return false;
	};
    };

    this.playSound = function(id){
	sm.stopAll();
        historyWidget.add(id);	
	var track = sm.getSoundById(id);
	if(!track){
	    track = self.createSound(id, api.radio.soundData[id].url);
	}
    	self.currentSound = track;
	self.currentSound.play();

	volume.conf(track);
	//TODO destroy this fucking hack
	if(volume.muted)
	    track.mute();

	if(!widget.pRew.hasClass('pgbPointer')){
	    widget.pRew.addClass('pgbPointer');
	};
    };

    this.event = {
	onplay: function(){
	    if (self.currentSound){
		var id = self.currentSound.sID;
		widget.title.text(self.soundData[id].title);
		widget.artist.text(self.soundData[id].artist);
		widget.play.attr('src', '/images/pausebtn2.png');
	    }
	}, 
	onfinish: function(){
	    api.playRandomTrack();
	},
	whileplaying: function(){   
	    percents = self.currentSound.position/self.currentSound.durationEstimate*100;
	    pgb.setPlay(percents);
	    pgb.setPlayCounter(self.calculate(self.currentSound.position));
	    pgb.setTotalCounter(self.calculate(self.currentSound.durationEstimate));
	},
	whileloading: function(){
	    percents = self.currentSound.bytesLoaded/self.currentSound.bytesTotal*100;
	    pgb.setLoad(percents);
	},
	onload: function(success){
	    if(!success){
		message('Вы переключаете песни слишком часто.<br/> Пожалуйста подождите.');
		setTimeout("window.api.playRandomTrack()", 5000);
	    }
	    pgb.setTotalCounter(self.calculate(self.currentSound.duration));	    
	},
    onpause: function(){
        widget.play.attr('src', '/images/btn.png')
    },
    onresume: function(){
        widget.play.attr('src', '/images/pausebtn2.png')
    }
    }
}


function Api() {
    var self = this;
    bindEvents();
    this.radio = new Radio();

    this.go = function(track){
	self.radio.soundData[track.id] = track;
	self.radio.createSound(track.id, track.url);
	self.radio.playSound(track.id);
    }
    
    this.playRandomTrack = function(){
	var composition_track = '';
	if(playByComposition){
	    $.getJSON('/composition/'+self.radio.currentSound.sID,{'artists':artists.join(',')}, 
		      function(track){
			  self.go(track);
		      })
	}else{
	    $.getJSON('/radio', {'artists':artists.join(',')}, function(track){
		self.go(track);
	    })
	    .error(function(){
		message('Произошла ошибка.<br /> Пожалуйста подождите.');
		setTimeout("window.api.playRandomTrack()", 5000);
	    });
	}
    };
    
}

window.api = new Api();

// 698b99 checkout with rev button
