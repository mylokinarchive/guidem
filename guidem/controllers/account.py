# -*- coding: utf-8 -*-
import logging
from urlparse import urlparse
from urllib import urlencode
import urllib2
import string
from formencode import htmlfill

from pylons import request, response, session, tmpl_context as c
from pylons.controllers.util import abort, redirect_to
from pylons.decorators import validate

from guidem.lib.base import BaseController, render
from guidem.lib import helpers as h
from guidem.lib.auth import authentication, authorization

from guidem.model import form, meta
import guidem.model as model

import webhelpers.paginate as paginate

log = logging.getLogger(__name__)

class AccountController(BaseController):
    
    def __before__(self):        
        try:
            c.items_per_page = int(request.params.get('items_per_page', ''))
        except ValueError:
            c.items_per_page = None
        if c.items_per_page:
            session['items_per_page'] = c.items_per_page
            session.save()
        elif session.has_key('items_per_page'):
            c.items_per_page = session['items_per_page']
        else:
            c.items_per_page = session['items_per_page'] = 20
            session.save()    
            
    @authentication
    def account(self):
        c.pm = paginate.Page(request.environ['_auth'].pm, request.params.get('pm_page', 1), items_per_page=c.items_per_page)
        c.friends = paginate.Page(request.environ['_auth'].friends, request.params.get('friends_page', 1), items_per_page=c.items_per_page)
        c.friendship_requests = paginate.Page(request.environ['_auth'].friendship_requests, request.params.get('friends_req_page', 1), items_per_page=c.items_per_page)
        c.bookmarks = paginate.Page(request.environ['_auth'].bookmarks, request.params.get('bookmarks_page', 1), items_per_page=c.items_per_page)
        c.ignored = paginate.Page(request.environ['_auth'].ignored, request.params.get('ignored_page', 1), items_per_page=c.items_per_page)
        return render('/account/account.htm')

    @authentication
    def correspondence(self, username):
        if request.environ['_auth'].username==username:
            return redirect_to('show_account', username=request.environ['_auth'].username)
        if h.auth.Users.user_exists(username):
            c.correspondent = model.Account.get(username)
        else:
            abort(404)
        c.correspondence= paginate.Page(model.PM.correspondence(request.environ['_auth'].username, c.correspondent.username), request.params.get('pm_page', 1), items_per_page=c.items_per_page)
        return render('/account/correspondence.htm')        
    
    @authentication
    def pm(self):
        c.pm = paginate.Page(request.environ['_auth'].pm, request.params.get('pm_page', 1), items_per_page=c.items_per_page)
        return render('/account/pm.htm')    
    
    @authentication
    def bookmarks(self):
        c.bookmarks = paginate.Page(request.environ['_auth'].bookmarks, request.params.get('bookmarks_page', 1), items_per_page=c.items_per_page)
        return render('/account/bookmarks.htm')
    
    @authentication
    def friends(self):
        c.friends = paginate.Page(request.environ['_auth'].friends, request.params.get('friends_page', 1), items_per_page=c.items_per_page)
        return render('/account/friends.htm')

    @authentication
    def ignored(self):
        c.ignored = paginate.Page(request.environ['_auth'].ignored, request.params.get('ignored_page', 1), items_per_page=c.items_per_page)
        return render('/account/ignored.htm') 
    
    @authentication
    def requests(self):
        c.friendship_requests = paginate.Page(request.environ['_auth'].friendship_requests, request.params.get('friends_req_page', 1), items_per_page=c.items_per_page)
        return render('/account/requests.htm')

    @authentication
    def playlists(self):
        c.playlists = request.environ['_auth'].playlists
        return render('/account/playlists.htm')
    
    
    @authentication
    def updates(self):
        c.updates = {}
        for n, bookmark in enumerate(request.environ['_auth'].bookmarks):
            c.updates[n] = [bookmark, meta.Session.query(model.Comment).filter(model.Comment.page==bookmark.page).\
             filter(model.Comment.datetime>request.environ['_auth'].lastupdate).filter(model.Comment.datetime>bookmark.datetime).filter(model.Comment.username!=request.environ['_auth'].username)]
            for ignored_user in request.environ['_auth'].ignored:
                c.updates[n][1] = c.updates[n][1].filter(model.Comment.username!=ignored_user)            
            c.updates[n][1] = paginate.Page(c.updates[n][1].order_by(model.Comment.datetime.desc()),
                                                request.params.get('%s_page'%bookmark.id, 1), items_per_page=c.items_per_page)
        return render('/account/update.htm')
    
    @authentication
    def read_updates(self):
        request.environ['_auth'].update()
        session['flash'] = u'Обновления прочитаны'
        session.save()
        return redirect_to(request.referrer)
    
    @authentication
    def friends_timeline(self):
        c.comments = request.environ['_auth'].order_comments_by_datetime(request.environ['_auth'].timeline_friends_comments)
        if c.comments:
            c.comments = paginate.Page(c.comments, request.params.get('tlcomments_page', 1), items_per_page=c.items_per_page)
        c.bookmarks = request.environ['_auth'].order_bookmarks_by_datetime(request.environ['_auth'].timeline_friends_bookmarks)
        if c.bookmarks:
            c.bookmarks = paginate.Page(c.bookmarks, request.params.get('tlbookmarks_page', 1), items_per_page=c.items_per_page)        
        return render('/account/friends_timeline.htm')
    
    def show(self, username):
        c.user = model.Account.get(username)
        if c.user:
            if request.environ['_auth'] and request.environ['_auth'].username==username:
                return redirect_to('account')

            c.friends = paginate.Page(c.user.friends, request.params.get('friends_page', 1), items_per_page=c.items_per_page)
            c.bookmarks = paginate.Page(c.user.bookmarks, request.params.get('bookmarks_page', 1), items_per_page=c.items_per_page)

            c.timeline_comments = c.user.order_comments_by_datetime(c.user.timeline_comments)
            if c.timeline_comments.count():
                c.timeline_comments = paginate.Page(c.timeline_comments, request.params.get('tlcomments_page', 1), items_per_page=c.items_per_page)
            else:
                c.timeline_comments = None
            c.timeline_bookmarks = c.user.order_bookmarks_by_datetime(c.user.timeline_bookmarks)
            if c.timeline_bookmarks.count():
                c.timeline_bookmarks = paginate.Page(c.timeline_bookmarks, request.params.get('tlbookmarks_page', 1), items_per_page=c.items_per_page)        
            else:
                c.timeline_bookmarks = None
            
            c.playlists = c.user.playlists if c.user.playlists.count() else None
            
            c.is_admin = h.auth.Users.user_has_permission(c.user.username, 'admin') if h.auth.Users.user_exists(username) else False
            c.is_friend = model.Friends.is_friends(c.user.username, request.environ['_auth'].username) if request.environ['_auth'] else False
            c.is_ignored = model.Friends.is_ignore(request.environ['_auth'].username, c.user.username) if request.environ['_auth'] else False
            return render('/account/show.htm')
        else:                
            abort(404)
    
    @authentication
    def reset_password(self):
        return render('/account/resetpwd.htm')
    
    @authentication
    @validate(schema = form.ResetPassword(), form="reset_password")
    def reset_password_submit(self):
        old = self.form_result['old']
        new = self.form_result['new']
        if request.environ['_auth'].password!=old:
            session['flash-error'] = u'Неправильный старый пароль'
            session.save()
            return redirect_to(request.referrer)
        else:
            request.environ['_auth'].password = new
            meta.Session.commit()
            session['flash'] = u'Пароль установлен'
            session.save()
            return redirect_to('show_account', username=request.environ['_auth'].username)
    
    def signin(self):
        c.backref = request.params.get('backref', '')
        return render('/account/signin.htm')

    def signin_submit(self):
        username = request.params.get('username_signin', '').lower()
        password = request.params.get('password_signin', '')
        backref = request.params.get('backref', '').encode('utf-8')
        account = model.Account.get(username)
        if account and account.password==password:
            response.set_cookie('_auth', '%s:%s'%(username, password))
            return redirect_to(backref or '/')
        else:
            session['flash-error'] = u'Неправильная пара логин/пароль'
            session.save()
            return redirect_to('signin')
    
    def signout(self):
        if request.environ['_auth']:
            request.environ['_auth'].offline()
        response.delete_cookie('_auth')
        return redirect_to(request.referrer)

    def register(self):
        if session.get('recaptcha_error'):
            c.recaptcha_error = session['recaptcha_error']
            form = session['recaptcha_error_data']
            del session['recaptcha_error']
            del session['recaptcha_error_data']
            session.save()
        else:
            form = None
        return htmlfill.render(render('/account/register.htm'), defaults=form)
    
    @validate(schema=form.RegisterForm, form='register')
    def submit(self):
        c.username = self.form_result['username'].lower()
        avalible = '%s%s_'%(string.letters, string.digits)
        for letter in c.username:
            if letter not in avalible:
                session['flash-error'] = u'Пожалуйста, для указания имени пользователя используйте только латинские буквы, цифры и символ подчеркивания _'
                session.save()
                return redirect_to('register')

        recaptcha_data = dict()
        recaptcha_data['privatekey'] = '6LcLGcASAAAAAILr6wMwusU_TbM9R4DX9ZX0xGVL'
        recaptcha_data['remoteip'] = request.remote_addr
        recaptcha_data['challenge'] = self.form_result['recaptcha_challenge_field']
        recaptcha_data['response'] = self.form_result['recaptcha_response_field']\
            .encode('utf-8')
        if session.get('recaptcha_error'):
            recaptcha_data['error'] = session['recaptcha_error']
        recaptcha_response = urllib2.urlopen( \
            'http://www.google.com/recaptcha/api/verify', \
            urlencode(recaptcha_data)).read()
        recaptcha_status, recaptcha_error = recaptcha_response.split('\n')
        if recaptcha_status=='true':
            if session.get('recaptcha_error'):
                del session['recaptcha_error']
                del session['recaptcha_error_data']
            session.save()
        else:
            session['recaptcha_error'] = recaptcha_error
            session['recaptcha_error_data'] = self.form_result
            session.save()
            return redirect_to('register')
        
        email = self.form_result['email']
        password = self.form_result['password']
        if model.Account.get_by_email(email).count():
            session['flash-error'] = u'Email адрес уже зарегистрирован'
            session.save()
            return redirect_to('register')
        if not h.auth.Users.user_exists(c.username):
            model.Account(c.username, password, email).db_add()
        else:
            session['flash-error'] = u'Пользователь с таким именем уже существует'
            session.save()
            return redirect_to('register')
        response.set_cookie('_auth', '%s:%s'%(c.username, password))        
        return redirect_to('account')
    
    @authentication
    def read_bookmark(self):
        page = request.params.get('page')        
        bookmark = model.Bookmark.get_unique(page, request.environ['_auth'].username)
        if bookmark:
            bookmark.datetime = model.now()()
            bookmark.db_add()
            #Hosting adaptation
            page = request.referrer
            page = page.encode('utf-8').replace('/webapp', '')
            return redirect_to(page)
        else:
            return abort(404)
        
    @authentication
    def add_to_playlist(self):
        playlist_title = request.params.get('title', '').lower()
        try:
            track = int(request.params.get('track'))
        except ValueError:
            return abort(404)            
        if not track or not playlist_title:
            return abort(404)

        playlist = model.Playlist.get_unique(playlist_title, request.environ['_auth'].username)
        if playlist_title==u'_new' or not track or not playlist:
            return redirect_to('create_playlist', track=track)
        else:
            track = model.Track.get_by_id(track)
            try:
                playlist.tracks.append(track)
                meta.Session.commit()
            except:
                session['flash-error'] = u'Песня уже добавлена в список воспроизведения'
                session.save()
            else:
                session['flash'] = u'Добавлено в список воспроизведения'
                session.save()
            return redirect_to('song', id=track.id)            
        
    @authentication
    def remove_from_playlist(self):
        playlist_title = request.params.get('title', '').lower()
        try:
            track = request.params.get('track', '')
        except ValueError:
            return abort(404)            
        if not track or not playlist_title:
            return abort(404)

        playlist = model.Playlist.get(title=playlist_title, username=request.environ['_auth'].username).one()
        track = model.Track.get_by_id(track)
        playlist.tracks.remove(track)
        try:
            meta.Session.commit()
        except:
            session['flash-error'] = u'Ошибка удаления песни из списка воспроизведения'
            session.save()
            return abort(403)
        else:
            session['flash'] = u'Песня удалена из списка воспроизведения'
            session.save()
            return redirect_to('playlist', title=playlist_title, username=request.environ['_auth'].username)            
        
    @authentication
    def create_playlist(self):
        try:
            c.track = int(request.params.get('track'))
        except ValueError:
            return abort(404)            
        return render('/account/create_playlist.htm')
    
    @authentication
    def create_playlist_submit(self):
        playlist_title = request.params.get('title', '').lower()
        try:
            track = int(request.params.get('track'))
        except ValueError:
            return abort(404)            
        if not track or not playlist_title:
            return abort(404)
        playlist = model.Playlist.get_unique(playlist_title, request.environ['_auth'].username)
        track = model.Track.get_by_id(track)
        if not playlist and track:
            playlist = model.Playlist(playlist_title, request.environ['_auth'].username)
            playlist.db_add()
            playlist.tracks.append(track)
            try:
                meta.Session.commit()
            except:
                session['flash-error'] = u'Плейлист не создан'
                return abort(403)
            else:
                session['flash'] = u'Плейлист создан'
        else:
            session['flash-error'] = u'Плейлист уже существует'
        session.save()
        return redirect_to('playlist', title=playlist.title, username=request.environ['_auth'].username)
            
    @authentication
    def remove_playlist(self):
        playlist_title = request.params.get('title', '').lower()
        
        playlist = model.Playlist.get_unique(playlist_title, request.environ['_auth'].username)
        if playlist and playlist.db_remove():
            session['flash'] = u'Список воспроизведения удален'
            session.save()
        else:
            return abort(404)            
        return redirect_to('account')

    @authentication
    def info(self):
        form = {}
        form['info'] = request.environ['_auth'].info
        return htmlfill.render(render('/account/information.htm'), defaults=form)
    
    @authentication
    def info_submit(self):
        info = request.params.get('info', '')
        if info:
            info = info.replace('<script', '')
            request.environ['_auth'].info = info
            if request.environ['_auth'].db_add():
                session['flash'] = u'Информация отредактированна'
            else:
                session['flash-error'] = u'Информация не отредактирована'
            session.save()
            return redirect_to('account')
        else:
            return abort(404)
#Copyright 2010 Andrey Gubarev
