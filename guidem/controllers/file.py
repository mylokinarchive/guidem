# -*- coding: utf-8 -*-
import logging
import os
import datetime

import boto
from pylons import request, response, session, tmpl_context as c
from pylons.controllers.util import abort, redirect_to
from pylons.decorators import validate

from guidem.lib.base import BaseController, render
from guidem.lib import helpers as h
from guidem.model import meta
import guidem.model as model

from guidem.lib.auth import authentication, authorization

log = logging.getLogger(__name__)



class FileController(BaseController):
                
    def track(self, id):                 
        radio_delay = session.get('radio_delay')
        if radio_delay and (datetime.datetime.now()-radio_delay)<datetime.timedelta(seconds=3):
            return abort(403)
        session['radio_delay'] = datetime.datetime.now()
        session.save()
        
        track = model.Track.get_by_id(id)
        if not track:
            return abort(404)
        if track.fileobj.path[:4]==u"aws:":
            conn = boto.connect_s3('AKIAJ5TBK4KX3L3HJE3A', 'KlMgimVql5cTAuRMp8jdCrvWr+9Q7+tOEzkO9wam')
            key = conn.get_bucket('guidem').get_key(track.fileobj.path[4:])
            if key:
                return redirect_to(key.generate_url(3600, force_http=True))
            else:
                return abort(404)
        else:
            headers = []
            headers.append(('content-length', str(track.fileobj.contentlength)))
            headers.append(('content-disposition', 'attachment; filename="%s-%s.mp3"'%(track.artistobj.name.encode('utf-8'), track.title.encode('utf-8'))))
            headers.append(('content-type', 'audio/mpeg'))
            self.start_response('200 OK', headers)
            try:
                f = open(track.fileobj.path.encode('utf-8'), 'rb')
                return f
            except IOError:
                return abort(404)
            
    
    def image(self, id):
        image = model.Image.get_by_id(id)
        if not image:
            return abort(404)
        response.content_type = 'image/jpeg'
        try:
            f = open(image.fileobj.path.encode('cp1251'), 'rb')
            return f
        except:
            return abort(404)

#Copyright 2010 Andrey Gubarev
