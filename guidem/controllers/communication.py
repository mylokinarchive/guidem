# -*- coding: utf-8 -*-
import logging
import re

from pylons import request, response, session, tmpl_context as c
from pylons.controllers.util import abort, redirect_to
from pylons.decorators import validate

from guidem.lib.base import BaseController, render

from guidem.lib import helpers as h
from guidem.lib.auth import authentication, authorization
from guidem.model import form, meta
import guidem.model as model

from urlparse import urlparse

log = logging.getLogger(__name__)

class CommunicationController(BaseController):
    
    @authentication
    def add_comment(self):
        text = request.params.get('text', '').lower()
        page = request.params.get('page', '')
        comment = model.Comment(request.environ['_auth'].username, text, page).db_add()
        if comment:
            session['flash'] = u'Комментарий добавлен'
            session.save()
        else:
            session['flash-error'] = u'Комментарий не добавлен'
            session.save()   
        return redirect_to(request.referrer)
   
    @authentication    
    def remove_comment(self):
        comment = model.Comment.get_by_id(request.params.get('comment', ''))
        if comment and request.environ['_auth'].username!=comment.username:
            abort(403)
        if comment and comment.db_remove():
            session['flash'] = u'Комментарий удален'
        else:
            session['flash-error'] = u'Комментарий не удален'
        session.save()
        return redirect_to(request.referrer)
    
    @authentication
    def pm(self):
        username = request.params.get('account', '')
        if request.environ['_auth'].username==username:
            session['flash-error'] = u'Нельзя отправлять самому себе'
            session.save()
            return redirect_to(request.referrer)
        text = request.params.get('text', '')
        if h.auth.Users.user_exists(username) and text:
            if model.Friends.is_ignore(request.environ['_auth'].username, username):
                session['flash-error'] = u'Игнорируемые пользователи не могут отправлять сообщения'
            else:
                pm = model.PM(request.environ['_auth'].username, username, text).db_add()
                session['flash'] = u'Личное сообщение отправлено'
            session.save()
            return redirect_to(request.referrer)
        else:
            abort(404)

    @authentication
    def invite(self):
        c.page = model.Page.get_by_id(request.params.get('page'))
        if c.page:
            return render('/account/invite.htm')
        else:
            return abort(404)
    
    @authentication     
    def invite_submit(self):
        id = unicode(request.params.get('page', ''))
        page = model.Page.get_by_id(id)
        if not page:
            session['flash-error'] = u'Целевая страница не существует'
            session.save()        
            abort(404)
            
        text = request.params.get('text', u'Приглашение.')
        friends = request.environ['_auth'].friends[:]
        for username in set(request.params.values()):
            if username in friends:
                if not model.PM(request.environ['_auth'].username, username, text, page).db_add():
                    session['flash-error'] = u'Приглашения отправлены с ошибками'
        session['flash'] = u'Приглашения отправлены'
        session.save()
        #Hosting adaptation
        path = request.referrer
        path = path.encode('utf-8').replace('/webapp', '')
        return redirect_to(path)

    @authentication
    def add_bookmark(self):
        #TODO validate.
        id = request.params.get('page', '')
        page = model.Page.get_by_id(id)
        if page and model.Bookmark(page.id, request.environ['_auth'].username).db_add():
            session['flash'] = u'Добавлено в закладки'               
        else:
            session['flash-error'] = u'Ошибка, добавления в закладки'
        session.save()
        return redirect_to(request.referrer)
    
    @authentication
    def remove_bookmark(self):
        id = request.params.get('page', '')
        bookmark = model.Bookmark.get_unique(id, request.environ['_auth'].username)
        if bookmark and bookmark.db_remove():
            session['flash'] = u'Закладка удалена'               
        else:
            session['flash-error'] = u'Закладка не удалена'
        session.save()
        return redirect_to(request.referrer)

    @authentication
    def add_friend(self):
        friend = request.params.get('id', '')
        friend = model.Account.get(friend)    
        if friend and model.Friends(request.environ['_auth'].username, friend.username).db_add():
            if model.Friends.is_friends(request.environ['_auth'].username, friend.username):
                session['flash'] = u'%s теперь ваш друг'%friend.username
            else:
                session['flash'] = u'Запросу отправлен'
        else:
            if model.Friends.is_friends(request.environ['_auth'].username, friend.username):
                session['flash-error'] = u'%s и %s уже друзья'%(request.environ['_auth'].username, friend.username)
            else:
                session['flash-error'] = u'Запросу уже был отправлен'
        session.save()
        return redirect_to(request.referrer)
        
    @authentication
    def remove_friend(self):
        friend = request.params.get('id', '')
        friend = model.Account.get(friend)
        if not friend:
            abort(403)
        friendship = model.Friends.get(username1=request.environ['_auth'].username, username2=friend.username)
        if friendship.count() and friendship.one().db_remove():
            session['flash'] = u'%s больше не ваш друг'%friend.username
            session.save()
        return redirect_to(request.referrer)

    @authentication
    def ignore(self):
        friend = request.params.get('id', '')
        friend = model.Account.get(friend)
        ignore = True if request.params.get('ignore', False) else False
        if not friend:
            abort(403)
        friendship = model.Friends.get(username1=request.environ['_auth'].username, username2=friend.username)
        if friendship.count():
            friendship = friendship.one()
            friendship.ignore = ignore
        else:
            friendship = model.Friends(request.environ['_auth'].username, friend.username, ignore)
        if ignore:
            session['flash'] = u'Вы игнорируете %s'%friend.username
            friendship.db_add()
        else:
            session['flash'] = u'Вы больше не игнорируете %s'%friend.username
            friendship.db_remove()
        session.save()
        return redirect_to(request.referrer)
    
    @authentication
    def mark_as_spam(self):
        comment = model.Comment.get_by_id(request.params.get('id', ''))
        if comment and model.SpamMark(request.environ['_auth'], comment).db_add():
            comment.spam+=1
            comment.db_add()
            session['flash'] = u'Сообщение помечено как спам'
        else:
            session['flash-error'] = u'Сообщение не помечено как спам'
        session.save()
        return redirect_to(request.referrer)
                    