# -*- coding: utf-8 -*-
import itertools
import logging

from pylons import request, response, session, tmpl_context as c
from pylons.controllers.util import abort, redirect_to

from formencode import htmlfill

from guidem.lib.base import BaseController, render
from guidem.lib import helpers as h
from guidem.model import meta
import guidem.model as model

from sqlalchemy import func
from sqlalchemy import and_, or_
import webhelpers.paginate as paginate

log = logging.getLogger(__name__)

class PageController(BaseController):
    
    def __before__(self):
        request.environ['_auth'] = request.environ['_auth']

        try:
            c.items_per_page = int(request.params.get('items_per_page', ''))
        except ValueError:
            c.items_per_page = None
        if c.items_per_page:
            session['items_per_page'] = c.items_per_page
            session.save()
        elif session.has_key('items_per_page'):
            c.items_per_page = session['items_per_page']
        else:
            c.items_per_page = session['items_per_page'] = 20
            session.save()
            
    def song(self, id):
        c.track = model.Track.get_by_id(id or '')
        if c.track:
            c.page = model.Page.get_or_add(u'track', c.track.id)
            c.comments = paginate.Page(model.Comment.get(page = c.page.id).order_by(model.Comment.datetime.asc()),\
                request.params.get('comments_page', 1), items_per_page=c.items_per_page)
            c.watchers = paginate.Page(model.Bookmark.watching(c.page.id),\
                request.params.get('watchers_page', 1), items_per_page=c.items_per_page)
            return render('/page/song.htm')
        else:
            abort(404)

    def artist(self, id):
        c.artist = model.Artist.get_by_id(id or '')
        if c.artist:
            c.page = model.Page.get_or_add(u'artist', c.artist.id)
            c.comments = paginate.Page(model.Comment.get(page = c.page.id).order_by(model.Comment.datetime.asc()),\
                request.params.get('comments_page', 1), items_per_page=c.items_per_page)
            c.watchers = paginate.Page(model.Bookmark.watching(c.page.id),\
                request.params.get('watchers_page', 1), items_per_page=c.items_per_page)
            return render('/page/artist.htm')
        else:
            abort(404)

    def discussion(self, id):
        c.discus = model.Discus.get_by_id(id or '')
        if c.discus:            
            c.page = model.Page.get_or_add(u'discussion', c.discus.id)
            c.comments = paginate.Page(model.Comment.get(page = c.page.id).order_by(model.Comment.datetime.asc()),\
                request.params.get('comments_page', 1), items_per_page=c.items_per_page)
            c.watchers = paginate.Page(model.Bookmark.watching(c.page.id),\
                request.params.get('watchers_page', 1), items_per_page=c.items_per_page)
            c.chattering = paginate.Page(model.Comment.get(page = c.page.id).group_by(model.Comment.username).all(),\
                request.params.get('chattering_page', 1), items_per_page=c.items_per_page) if model.Comment.get(page = c.page.id).count() else []                        
            return render('/page/discus.htm')
        else:
            return abort(404)
    
    def search(self):
        def search(model, field, query):
            query = query.split()
            sql_query = meta.Session.query(model)
            for n in xrange(len(query), 1, -1):
                conditions = []
                for combination in itertools.combinations(query, n):
                    condition = and_(*map(lambda x:field.like('%'+x+'%'), combination))
                    subresult = sql_query.filter(condition)
                    if subresult.count():
                        conditions.append(condition)
                if conditions:
                    result = sql_query.filter(or_(*conditions))
                    return result
            return sql_query.filter(field.like('%'+' '.join(query)+'%'))
            
        c.query = request.params.get('query', '')
        c.artists = search(model.Artist, model.Artist.name, c.query)
        c.tracks = search(model.Track, model.Track.title, c.query)
        c.discus = meta.Session.query(model.Discus).filter(model.Discus.title.like('%'+c.query+'%'))
        c.accounts = meta.Session.query(model.Account).filter(model.Account.username.like('%'+c.query+'%'))
        return htmlfill.render(render('/page/search.htm'), {'query': c.query})
    
    def playlist(self, username, title):
        title = unicode(title).lower()
        if not title or not username:
            return abort(403)
        c.playlist = model.Playlist.get_unique(title, username)
        if c.playlist:
            c.page = model.Page.get_or_add(u'playlist', c.playlist.id)
            return render('/page/playlist.htm')
        else:
            return abort(404)
    
    def playlistn(self, id):
        playlist = model.Playlist.get_by_id(id)
        if playlist:
            return redirect_to('playlist', username=playlist.username, title=playlist.title)
        else:
            return abort(404)

    def news(self, id):
        c.news = model.News.get_by_id(id)
        if c.news:
            c.page = model.Page.get_or_add(u'news', c.news.id)            
            c.comments = paginate.Page(model.Comment.get(page = c.page.id).order_by(model.Comment.datetime.asc()),\
                request.params.get('comments_page', 1), items_per_page=c.items_per_page)
            c.watchers = paginate.Page(model.Bookmark.watching(c.page.id),\
                request.params.get('watchers_page', 1), items_per_page=c.items_per_page)
            c.chattering = paginate.Page(model.Comment.get(page = c.page.id).group_by(model.Comment.username).all(),\
                request.params.get('chattering_page', 1), items_per_page=c.items_per_page) if model.Comment.get(page = c.page.id).count() else []                        
            return render('/page/news.htm')
        else:
            return abort(404)
        
#Copyright 2010 Andrey Gubarev
