# -*- coding: utf-8 -*-
import logging

from pylons import request, response, session, tmpl_context as c
from pylons.controllers.util import abort, redirect_to
from pylons.decorators import validate

from guidem.lib.base import BaseController, render
from pylons.templating import render_mako_def

from guidem.model import meta, form
from guidem.lib import helpers as h
from guidem.lib.auth import authentication, authorization
import guidem.model as model

import webhelpers.paginate as paginate
from sqlalchemy import func
from sqlalchemy import or_

import datetime
from random import randint
import simplejson as json
from guidem.lib import PyRSS2Gen


log = logging.getLogger(__name__)

class BaseController(BaseController):
    
    def __before__(self):        
        request.environ['_auth'] = request.environ['_auth']
        
        try:
            c.items_per_page = int(request.params.get('items_per_page', ''))
        except ValueError:
            c.items_per_page = None
        if c.items_per_page:
            session['items_per_page'] = c.items_per_page
            session.save()
        elif session.has_key('items_per_page'):
            c.items_per_page = session['items_per_page']
        else:
            c.items_per_page = session['items_per_page'] = 10
            session.save()            
    
    def home(self):
        def timeline():
            def stream(model):
                for instance in meta.Session.query(model).order_by(model.datetime.desc()):
                    yield instance
            
            streams = map(stream, [model.Track, model.News, model.Account])
            stats = [stream.next() for stream in streams]
            while stats:
                datetime, latest_element = max([(stat.datetime, stat) for stat in stats])
                index = stats.index(latest_element)
                try:
                    stats[index] = streams[index].next()
                except StopIteration:
                    del streams[index]
                    del stats[index]
                yield latest_element
        
        def sets(timeline):
            class Group(list):
                _types = {model.Account: 'account_header',
                    model.News: 'news_header',
                    model.Track: 'track_header'
                    }

                def __init__(self):
                    self.hidden = []
                    super(list, self).__init__()

                def header(self):
                    assert len(self)>0, "Empty group"
                    for t in self._types:
                        if isinstance(self[0], t):
                            template = self._types[t]
                            return render_mako_def('timeline_widgets.html', template, num=self.length, datetime=self[0].datetime)
                    else:
                        return
                @property
                def length(self):
                    return len(self)+len(self.hidden)

            groupes = [Group()]
            while len(groupes)<10:
                try:
                    element = timeline.next()
                except StopIteration:
                    break
                group = groupes[-1]
                if len(group):
                    if type(group[0])==type(element) and \
                       group[0].datetime.day==element.datetime.day:
                        if len(group)<10:
                            group.append(element)
                        else:
                            group.hidden.append(element)
                    else:
                        group = Group()
                        group.append(element)
                        groupes.append(group)
                else:
                    group.append(element)
            return groupes           
            
        c.timeline = sets(timeline())
        c.tracks = meta.Session.query(model.Track)
        total_tracks = c.tracks.count()
        c.tracks = [c.tracks[randint(0, total_tracks-1)] for x in xrange(3)]
        
        c.news = model.News.get().order_by(model.News.datetime.desc()).limit(10)
        c.accounts = meta.Session.query(model.Account).order_by(model.Account.datetime.desc())
        c.comments = meta.Session.query(model.Comment).filter(model.Page.type==u'discussion')\
            .order_by(model.Comment.datetime.desc())
        return render('/index.htm')

    def player(self):
        c.artists = model.Artist.get().order_by(model.Artist.name)
        return render('/radio.htm')

    def radio(self):
        tracks = meta.Session.query(model.Track)
        artists = request.params.get('artists')
        if artists:
            artists = [int(artist_id) for artist_id in artists.split(',')]
            tracks = tracks.filter(or_(*map(lambda x:model.Track.artist==x, artists)))
        n = randint(0, tracks.count()-1)
        track = tracks[n]
        return self.radio_json(track)

    def composition(self, tid):
        track = model.Track.get_by_id(tid)
        if not track:
            return abort(404)

        artists = request.params.get('artists', [])
        if artists:
            artists = [int(artist_id) for artist_id in artists.split(',')]

        track = track.next_by_composition()
        if (track and (artists and track.artist in artists)) or (track and not artists):
            return json.dumps({'id':track.id, 'title':track.title,\
                            'artist':track.artistobj.name, 'url':h.url_for('track', id=track.id)})
        else:
            tracks = meta.Session.query(model.Track).filter(or_(model.Track.no==0, model.Track.no==1))
            if artists:
                tracks = tracks.filter(or_(*map(lambda x:model.Track.artist==x, artists)))
            if tracks.count():
                tracks = tracks.order_by(model.Track.id)
                n = randint(0, tracks.count()-1)
                track = tracks[n]
                return self.radio_json(track)
            else:
                return self.radio()                

    def radio_json(self, track):
        return json.dumps({'id':track.id, 'title':track.title, \
                               'artist':track.artistobj.name, \
                               'track_url': h.url_for('song', id=track.id), \
                               'artist_url':h.url_for('artist', id=track.artist), \
                               'composition':track.composition, 
                           'url':h.url_for('track', id=track.id)})

        

    def track_info(self, id):
        track = model.Track.get_by_id(id)
        if track:
            return json.dumps({'track':track.title, 'artist':track.artistobj.name, 'url':h.url_for('track', id=track.id)})
        else:
            return abort(404)
        
    def track_render(self, id):
        C.track = model.Track.get_by_id(id)
        if c.track:
            return render('/ajax/player_track.htm')
        else:
            return abort(404)        

    def track_history_render(self, id):
        c.track = model.Track.get_by_id(id)
        if c.track:
            return render('/ajax/player_history_track.htm')
        else:
            return abort(404)          
    
    def tracks(self):
        c.tracks = paginate.Page(meta.Session.query(model.Track).all(),\
            request.params.get('page', 1), items_per_page=c.items_per_page)
        return render('/tracks.htm')

    def tracks_letter(self, letter):
        c.letter = letter.upper()
        c.tracks = paginate.Page(meta.Session.query(model.Track).filter(model.Track.title.like(c.letter+'%')),\
            request.params.get('page', 1), items_per_page=10)
        return render('/tracks.htm')
    
    
    def artists(self):
        c.artists = paginate.Page(meta.Session.query(model.Artist).all(),\
            request.params.get('page', 1), items_per_page=c.items_per_page)
        return render('/artists.htm')

    def artists_letter(self, letter):
        c.letter = letter.upper()
        c.artists = paginate.Page(meta.Session.query(model.Artist).filter(model.Artist.name.like(c.letter+'%')),\
            request.params.get('page', 1), items_per_page=c.items_per_page)
        return render('/artists.htm')    

    def discussions(self):
        c.discussions = paginate.Page(meta.Session.query(model.Discus).order_by(model.Discus.datetime.desc()),\
            request.params.get('page', 1), items_per_page=c.items_per_page)        
        return render('/discussions.htm')


    @authentication
    @validate(schema = form.DiscussionForm(), form="discussions")
    def add_discussion(self):
        discus = model.Discus(self.form_result['title'], self.form_result['theme']).db_add()
        if discus:
            session['flash'] = u'Дискуссия создана.'
            session.save()
            return redirect_to('discus', id=discus.id)
        else:
            session['flash-error'] = u'Дискуссия не создана.'
            session.save()
            return redirect_to('discussions')
        
    
    def timeline(self):
        c.accounts = paginate.Page(meta.Session.query(model.Account).order_by(model.Account.datetime.desc()),\
            request.params.get('accounts_page', 1), items_per_page=c.items_per_page)
        c.tracks = paginate.Page(meta.Session.query(model.Track).order_by(model.Track.datetime.desc()),\
            request.params.get('tracks_page', 1), items_per_page=c.items_per_page)
        c.artists = paginate.Page(meta.Session.query(model.Artist).order_by(model.Artist.datetime.desc()),\
            request.params.get('artists_page', 1), items_per_page=c.items_per_page)
        return render('/timeline.htm')
    
    def timeline_accounts(self):
        c.accounts = paginate.Page(meta.Session.query(model.Account).order_by(model.Account.datetime.desc()),\
            request.params.get('accounts_page', 1), items_per_page=c.items_per_page)
        return render('/timeline.htm')
    
    def timeline_tracks(self):
        c.tracks = paginate.Page(meta.Session.query(model.Track).order_by(model.Track.datetime.desc()),\
            request.params.get('tracks_page', 1), items_per_page=c.items_per_page)
        return render('/timeline.htm')
    
    def timeline_artists(self):
        c.artists = paginate.Page(meta.Session.query(model.Artist).order_by(model.Artist.datetime.desc()),\
            request.params.get('artists_page', 1), items_per_page=c.items_per_page)
        return render('/timeline.htm')        
    
    def feed(self):
        news = model.News.get().order_by(model.News.datetime.desc()).limit(5)
        rss = PyRSS2Gen.RSS2(
            title = u"Guidem Новое",
            link = h.url_for('feed'),
            description = u"Обновления на сайте",
            
            lastBuildDate = datetime.datetime.now(),
            
            items = [PyRSS2Gen.RSSItem(title=item.title, description=item.text, pubDate=item.datetime, guid="http://%s%s"%(request.host, h.url_for('news', id=item.id))) for item in news]
            )
        return rss.to_xml('utf-8')

#Copyright 2010 Andrey Gubarev
