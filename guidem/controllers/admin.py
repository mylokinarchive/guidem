# -*- coding: utf-8 -*-
import logging
from formencode import htmlfill

from pylons import request, response, session, tmpl_context as c
from pylons.controllers.util import abort, redirect_to
from pylons.decorators import validate

from guidem.lib.base import BaseController, render
from guidem.lib import helpers as h
from guidem.lib.auth import authentication, authorization

from guidem.model import form, meta
import guidem.model as model

import webhelpers.paginate as paginate

import os
import hashlib
import StringIO
import urllib2
from guidem.lib import id3reader

log = logging.getLogger(__name__)

class AdminController(BaseController):

    def __before__(self):        
        try:
            c.items_per_page = int(request.params.get('items_per_page', ''))
        except ValueError:
            c.items_per_page = None
        if c.items_per_page:
            session['items_per_page'] = c.items_per_page
            session.save()
        elif session.has_key('items_per_page'):
            c.items_per_page = session['items_per_page']
        else:
            c.items_per_page = session['items_per_page'] = 20
            session.save()
            
    @authorization('admin')
    def home(self):
        return render('/admin/admin.htm')

    @authorization('admin')
    def images(self):
        c.images = paginate.Page(model.Image.get(),\
            request.params.get('page', 1), items_per_page=c.items_per_page)
        return render('/admin/images.htm')
    
    @authorization('admin')
    def stream(self):
        c.comments = paginate.Page(meta.Session.query(model.Comment).filter(model.Comment.spam>0).order_by(model.Comment.datetime.desc()),\
            request.params.get('comments_page', 1), items_per_page=c.items_per_page)
        c.accounts = paginate.Page(meta.Session.query(model.Account).order_by(model.Account.datetime.desc()),\
            request.params.get('accounts_page', 1), items_per_page=c.items_per_page)                                  
        c.spamers = paginate.Page(meta.Session.query(model.Account).filter_by(spamer=True).order_by(model.Account.datetime.desc()),\
            request.params.get('spamers_page', 1), items_per_page=c.items_per_page)                                           
        c.tracks = paginate.Page(meta.Session.query(model.Track).filter(model.Track.username!=u'').order_by(model.Track.datetime.desc()),\
            request.params.get('tracks_page', 1), items_per_page=c.items_per_page)
        return render('/admin/stream.htm')
    
    @authentication
    def master_track(self):
        return render('/admin/add/master_track.htm')
    
    @authentication
    def master_track_submit(self):
        upload = request.params.get('file', '')        
        upload.file.seek(0)

        id3r = id3reader.Reader(upload.file)
        artist = id3r.getValue('performer').decode('cp1251') if id3r.getValue('performer') else u'Blank'
        title = id3r.getValue('title').decode('cp1251') if id3r.getValue('title') else u'Blank'
        
        if artist=='Blank' or title=='Blank':
            session['flash'] = u'Необходимо отредактировать теги'
            session.save()
        
        full_path = './guidem/public/music/upload/%s.mp3'%hashlib.md5(upload.file.read()).hexdigest()
        upload.file.seek(0)  
        try:            
            file = open(full_path, 'wb+')
            file.write(upload.file.read())
            file.close()
            contentlength = os.stat(full_path).st_size
        except:
            session['flash-error'] = u'Ошибка сохранения файла'
            session.save()
            return abort(403)
        
        file = model.File(unicode(upload.filename), unicode(full_path), contentlength).db_add()
        artist = model.Artist.get_or_add(artist)
        username = None if h.auth.Users.user_has_permission(request.environ['_auth'].username, 'admin') else request.environ['_auth'].username        
        track = model.Track(title, artist, file.id, username).db_add()
        if not track:
            session['flash-error'] = u'Песня уже существует'
            session.save()
            return redirect_to('master_track')
        else:
            meta.Session.add(track)
            meta.Session.commit()
            return redirect_to('song', id=track.id)        

    @authentication
    def master_track_url_submit(self):
        url = request.params.get('url', '').encode()
        title = request.params.get('title', '')
        artist = request.params.get('artist', '')        
        if not url:
            return abort(404)
        
        content = urllib2.urlopen(url).read()
        
        upload = StringIO.StringIO()
        try:
            upload.write(content)
        except IOError:
            session['flash-error'] = u'Невозможно выполнить операцию ввода вывода'
            session.save()           
            abort(403)
        
        id3r = id3reader.Reader(upload)
        artist = id3r.getValue('performer').decode('cp1251') or 'Blank'
        title = id3r.getValue('title').decode('cp1251') or 'Blank'
        
        if artist=='Blank' or title=='Blank':
            session['flash'] = u'Необходимо отредактировать теги'
            session.save()
        
        full_path = './guidem/public/music/upload/%s.mp3'%hashlib.md5(upload.read()).hexdigest()
        upload.seek(0)  
        try:
            file = open(full_path, 'wb+')
            file.write(upload.read())
            file.close()
            contentlength = os.stat(full_path).st_size
        except:
            session['flash-error'] = u'Ошибка сохранения файла'
            session.save()
            return abort(403)
        file = model.File(upload.filename, full_path, contentlength).db_add()
        artist = model.Artist.get_or_add(artist)
        username = None if h.auth.Users.user_has_permission(request.environ['_auth'].username, 'admin') else request.environ['_auth'].username        
        track = model.Track(title, artist, file.id, username).db_add()
        if not track:
            session['flash-error'] = u'Песня уже существует'
            session.save()
            return redirect_to('master_track')
        else:
            meta.Session.add(track)
            meta.Session.commit()
            return redirect_to('song', id=track.id)  

    @authorization('admin')
    def confirm_track(self):
        track = model.Track.get_by_id(request.params.get('id',''))
        if track and track.confirm():
            session['flash'] = u'Песня подтверждена'
        else:
            session['flash-error'] = u'Песня не подтверждена'
        session.save()
        return redirect_to('song', id=track.id)
            
        
    @authorization('admin')
    def remove_song(self):
        track = model.Track.get_by_id(request.params.get('id',''))
        if track and track.full_remove():
            session['flash'] = u'Песня удалена'
        else:
            session['flash-error'] = u'Песня не удалена'
        session.save()
        return redirect_to('tracks')

    @authorization('admin')
    def remove_artist(self):
        artist = model.Artist.get_by_id(request.params.get('id',''))
        if artist and artist.full_remove():
            session['flash'] = u'Исполнитель удален'
        else:
            session['flash-error'] = u'Исполнитель не удален'
        session.save()        
        return redirect_to('artists')    

    @authorization('admin')
    def remove_image(self):
        img = model.Image.get_by_id(request.params.get('id',''))
        if img and img.fileobj.db_remove() and img.db_remove():
            session['flash'] = u'Рисунок удален'
            session.save()        
            return redirect_to('/')
        else:
            session['flash-error'] = u'Рисунок не удален'
            session.save()        
            return redirect_to(request.referrer or 'home')        
        

    @authorization('admin')    
    def set_admin_role(self):
        username = request.params.get('username', '')        
        users = h.auth.Users
        if users.user_exists(username):
            if not ('admin' in map(str, request.environ['_auth'].permissions)):
                model.Permission(username, u'admin').db_add()
            session['flash'] = u'Функции администратора активированны'
            session.save()
            return redirect_to(request.referrer)
        else:
            session['flash-error'] = u'Пользователь не существует или заблокирован'
            session.save()            
            abort(404)
    
    @authorization('admin')    
    def remove_comment(self):
        comment = model.Comment.get_by_id(request.params.get('comment', ''))
        if comment and comment.db_remove():
            session['flash'] = u'Комментарий удален'
        else:
            session['flash-error'] = u'Комментарий не удален'
        session.save()
        return redirect_to(request.referrer)
    
    @authorization('admin')    
    def remove_discus(self):
        discus = model.Discus.get_by_id(request.params.get('id', u''))
        if discus and discus.full_remove():
            session['flash'] = u'Дискуссия удалена'
        else:
            session['flash-error'] = u'Дискуссия не удалена'
        session.save()
        return redirect_to('discussions')

    @authorization('admin')    
    def block(self):
        account = model.Account.get_by_id(request.params.get('id', ''))
        log.debug(account)
        if account and request.environ['_auth']!=account and account.username!='admin' and account.block():
            session['flash'] = u'Пользователь заблокирован'
        else:
            session['flash-error'] = u'Пользователь не заблокирован'
        session.save()
        return redirect_to('show_account', username=account.username)

    @authorization('admin')    
    def unblock(self):
        account = model.Account.get_by_id(request.params.get('id', ''))
        if account:
            account.blocked = False
            account.db_add()
            session['flash'] = u'Пользователь разблокирован'
        else:
            session['flash-error'] = u'Пользователь не разблокирован'
        session.save()
        return redirect_to('show_account', username=account.username)    

#Add Content

    @authorization('admin')
    def add_image(self):
        return render('/admin/add/image.htm')

    @authorization('admin')
    def add_image_submit(self):
        upload = request.params.get('file', '')
        description = request.params.get('description', '')
        title = request.params.get('title', '')

        full_path = './guidem/public/music/upload/%s.jpg'%hashlib.md5(upload.file.read()).hexdigest()
        upload.file.seek(0)  
        try:            
            file = open(full_path, 'wb+')
            file.write(upload.file.read())
            file.close()
            contentlength = os.stat(full_path).st_size
        except:
            session['flash-error'] = u'Ошибка сохранения файла'
            session.save()
            return abort(403)
        
        upload = model.File(unicode(upload.filename), unicode(full_path), contentlength).db_add()
        
        if not upload:
            session['flash-error'] = u'Загрузка файла не удалась'
            session.save()
            return abort(404)
        image = model.Image(title, description, upload).db_add()
        if not image:
            upload.remove(upload.id)
            session['flash-error'] = u'Рисунок не добавлен'
            session.save()
            return redirect_to('add_image')
        else:
            return redirect_to('image', id=image.id)

    @authorization('admin')
    def add_news(self):
        return render('/admin/add/news.htm')

    @authorization('admin')
    def add_news_submit(self):
        text = request.params.get('text', u'')
        title = request.params.get('title', u'')
        source = request.params.get('source', u'')
        if text and model.News(title, text, source).db_add():
            session['flash'] = u'Новость добавлена'
            session.save()
            return redirect_to('home')
        else:
            return redirect_to('add_news')
        
#Edit content
    @authorization('admin')
    def edit_artist(self):
        artist = model.Artist.get_by_id(request.params.get('id', ''))
        if artist:
            c.id = artist.id
            form = {}
            form['id'] = artist.id
            form['name'] = artist.name
            form['who'] = artist.who
            form['info'] = artist.info
            return htmlfill.render(render('/admin/edit/artist.htm'), defaults=form)
        else:
            abort(404)
            
    @authorization('admin')
    @validate(schema = form.ArtistForm(), form="edit_artist")
    def edit_artist_submit(self):
        artist = model.Artist.get_by_id(request.params.get('id', ''))
        if artist:
            c.id = artist.id
            artist.name = self.form_result['name']
            artist.who = self.form_result['who']
            artist.info = self.form_result['info']
            artist.datetime = model.now()()
            artist.db_add()
            session['flash'] = u'Исполнитель отредактирован'
            session.save()            
            return redirect_to('artist', id=artist.id)
        else:
            session['flash-error'] = u'Исполнитель не отредактирован'
            session.save()
            abort(404)

    @authentication
    def edit_track(self):
        track = model.Track.get_by_id(request.params.get('id', ''))
        if track:
            if not (request.environ['_auth'].username==track.username or ('admin' in map(str, request.environ['_auth'].permissions))):
                abort(403)
            c.id = track.id
            form = {}
            form['id'] = track.id
            form['title'] = track.title
            form['artist'] = track.artistobj.name
            return htmlfill.render(render('/admin/edit/track.htm'), defaults=form)
        else:
            abort(404)            
            
    @authentication
    @validate(schema = form.TrackEditForm(), form="edit_track")
    def edit_track_submit(self):
        track = model.Track.get_by_id(request.params.get('id', ''))
        if track:
            if not (request.environ['_auth'].username==track.username or ('admin' in map(str, request.environ['_auth'].permissions))):
                abort(403)
            c.id = track.id
            track.title = self.form_result['title']
            if track.artistobj.name!=self.form_result['artist']:                
                artist = model.Artist.get_or_add(self.form_result['artist'])
                track.artist = artist.id
            if self.form_result['file']:
                track.fileobj.full_remove()
                upload = request.params.get('file', '')
                full_path = './guidem/public/music/upload/%s.mp3'%hashlib.md5(upload.read()).hexdigest()
                upload.seek(0)  
                try:            
                    file = open(full_path, 'wb+')
                    file.write(upload.read())
                    file.close()
                    contentlength = os.stat(full_path).st_size
                except:
                    session['flash-error'] = u'Ошибка сохранения файла'
                    session.save()
                    return abort(403)
                file = model.File(upload.filename, full_path, contentlength).db_add()
                artist = model.Artist.get_or_add(artist)
                username = None if h.auth.Users.user_has_role(request.environ['_auth'].username, 'admin') else request.environ['_auth'].username        
                track = model.Track(title, artist, file.id, username).db_add()
            track.datetime = model.now()()
            track.db_add()
            session['flash'] = u'Песня отредактирована'
            session.save()            
            return redirect_to('song', id=track.id)
        else:
            session['flash-error'] = u'Песня не отредактирована'
            session.save()
            abort(404)

    @authorization('admin')
    def edit_discus(self):
        discus = model.Discus.get_by_id(request.params.get('id', ''))
        if discus:
            c.id = discus.id
            form = {}
            form['id'] = discus.id
            form['title'] = discus.title
            form['theme'] = discus.theme
            return htmlfill.render(render('/admin/edit/discus.htm'), defaults=form)
        else:
            abort(404)
            
    @authorization('admin')
    @validate(schema = form.DiscussionForm(), form="edit_discus")
    def edit_discus_submit(self):
        discus = model.Discus.get_by_id(request.params.get('id', ''))
        if discus:
            c.id = discus.id
            discus.title = self.form_result['title']
            discus.theme = self.form_result['theme']
            discus.datetime = model.now()()
            discus.db_add()
            session['flash'] = u'Дискусия отредактирована.'
            session.save()            
            return redirect_to('discus', id=discus.id)
        else:
            session['flash-error'] = u'Дискуссия не отредактирована'
            session.save()
            abort(404)   

    @authorization('admin')
    def edit_news(self):
        news = model.News.get_by_id(request.params.get('id', ''))
        if news:
            c.id = news.id
            form = {}
            form['id'] = news.id
            form['text'] = news.text
            form['title'] = news.title
            form['source'] = news.source
            return htmlfill.render(render('/admin/edit/news.htm'), defaults=form)
        else:
            abort(404)
            
    @authorization('admin')
    @validate(schema = form.NewsForm(), form="edit_news")
    def edit_news_submit(self):
        news = model.News.get_by_id(request.params.get('id', ''))
        if news:
            c.id = news.id
            news.text = self.form_result['text']
            news.title = self.form_result['title']
            news.source = self.form_result['source']
            news.datetime = model.now()()
            news.db_add()
            session['flash'] = u'Новость отредактирована'
            session.save()            
            return redirect_to('home')
        else:
            session['flash-error'] = u'Новость не отредактирована'
            session.save()
            abort(404)            

#Copyright 2010 Andrey Gubarev