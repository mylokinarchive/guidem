import os
import logging
import shutil

import boto

logging.basicConfig(level=logging.INFO)
__author__ = 'andrey'

class FSStorage(object):
    PATH = "/Users/andrey/Workspace/guidem/guidem/public/music/"

    def __init__(self):
        self.log = logging.getLogger("File system Storage")
        if not os.path.exists(self.PATH):
            self.log.error("Music folder doesnt exist: '%s'"%self.PATH)
            raise StorageError

    def upload(self, src_fp, fname=None):
        assert fname!=None, "FSStorage requires fname"

        path = os.path.join(self.PATH, fname)
        while os.path.exists(path):
            fname = "_%s"%fname
            path = os.path.join(self.PATH, fname)
        self.log.info("New file path: '%s'"%path)
        dest_fp = open(path, 'wb')
        self.log.debug("Start copy")
        #TODO set file cursor position to 0
        shutil.copyfileobj(src_fp, dest_fp)#TODO file write exception handling
        self.log.debug("Copy complete")
        dest_fp.close()
        self.log.info("New file created successful: '%s'"%path)
        return path

class HostingStorage(FSStorage):
    PATH = "/usr/home/mylokin/app/guidem/public/music/"

class LocalStorage(FSStorage):
    PATH = "/Users/andrey/Workspace/guidem/guidem/public/music/"

class LocalNotebookStorage(FSStorage):
    PATH = "/home/andrew/workspace/new_guidem/guidem/guidem/public/music"
    
class S3Storage(object):
    def __init__(self):
        self.log = logging.getLogger("S3 Storage")
        self.conn = boto.connect_s3('AKIAJ5TBK4KX3L3HJE3A', 'KlMgimVql5cTAuRMp8jdCrvWr+9Q7+tOEzkO9wam')
        self.bucket = self.conn.get_bucket('guidem')

    def upload(self, fp, key_name=None):
        if not key_name:
            key_name = boto.s3.key.compute_md5(fp)
            fp.seek(0)#work?
        key = self.bucket.get_key(key_name)
        while key:
            key = self.bucket.get_key("_%s"%key_name)
        key = self.bucket.new_key(key_name)
        key.set_contents_from_file(fp, reduced_redundancy=True)
        return "aws:%s"%key_name
        

class StorageError(Exception):
    pass
