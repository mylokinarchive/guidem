import boto
import db
import base64

class Merge(object):
    
    def __init__(self, db):
        self.db = db
        conn = boto.connect_s3('AKIAJ5TBK4KX3L3HJE3A','KlMgimVql5cTAuRMp8jdCrvWr+9Q7+tOEzkO9wam')
        self.bucket = conn.get_bucket('guidem')

    def _fresh(self):
        results = self.bucket.list('music/')
        for r in results:
            yield r

    def process(self):
        fresh = self._fresh()
        fresh.next()
        for key in fresh:
            key = self.bucket.get_key(key.name)
            artist_ = key.get_metadata('artist')
            artist = artist_.encode('utf-8').replace(' ', '+')
            artist = base64.b64decode(artist).decode('utf-8')
            title_ = key.get_metadata('title')
            title = title_.encode('utf-8').replace(' ', '+')
            title = base64.b64decode(title).decode('utf-8')
            self.m(key, artist, title)
            print "%s %s"%(artist, title)

    def m(self, key, artist, title, contentlength=0):
        assert key.name[:6]=="music/", "Key from not supported folder"
        key_name = 'archive/%s'%key.name[6:]
        path = ('aws:%s'%key_name).decode('utf-8')
        if self.db.update(path, contentlength, artist, title):
            archive_key = key.copy('guidem', key_name)
            key.delete()
            return True
        else:
            return False

if __name__=='__main__':
    merge = Merge(db.DB())
    merge.process()
    #db_ = db.LocalDB()
    #db_.update(u'/jopa/142345.mp3', 313, u'Arti21st2 yep', u'Some 3t3itle')
    
