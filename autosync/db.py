import logging
import os
import sys

logging.basicConfig(level=logging.INFO)
log = logging.getLogger("Database")
__author__ = 'andrey'

class DB(object):
    CONFIG = "config:/home/mylokin/guidem/src/production.ini"
    PATH = "/home/mylokin/guidem/src/"

    def __init__(self):
        os.chdir(self.PATH)
        sys.path.insert(0, self.PATH)
        from paste.deploy import appconfig
        from guidem.config.environment import load_environment
        conf = appconfig(self.CONFIG)
        load_environment(conf.global_conf, conf.local_conf)
        log.info("Database init: %s"%self.CONFIG)
        
    def update(self, path, contentlength, artist, title):
        import guidem.model
        
        assert isinstance(path, unicode), "Path must be unicode"
        assert isinstance(artist, unicode), "Artist must be unicode"
        assert isinstance(title, unicode), "Title must be unicode"

        fname = os.path.basename(path)
        file = guidem.model.File(fname, path, contentlength).db_add()
        if file:
            artist = guidem.model.Artist.get_or_add(artist)
            track = guidem.model.Track(title, artist, file.id)
            status = track.db_add()
            if status:
                log.info("Track '%s' - '%s' created"%(artist.name, track.title))
                return True
            else:
                log.error("Track '%s' - '%s' not created."%(artist.name, track.title))
                file.db_remove()
                return False
        else:
            log.error("File '%s' not created"%fname)
            return False

class LocalDB(DB):
    CONFIG = "config:/Users/andrey/Workspace/guidem/development.ini"
    PATH = "/Users/andrey/Workspace/guidem/"

class LocalNotebookDB(DB):
    CONFIG = "config:/home/andrew/workspace/new_guidem/guidem/development.ini"
    PATH = "/home/andrew/workspace/new_guidem/guidem/"
    
class HostingDB(DB):
    CONFIG = "config:/home/mylokin/guidem/src/production.ini"
    PATH = "/home/mylokin/guidem/src/"

if __name__=="__main__":
    pass
    #db = LocalDB()
    #db.update(u'/jopa/142345.mp3', 313, u'Arti21st2 yep', u'Some 3t3itle')
