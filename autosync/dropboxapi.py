import os

import dropbox.auth
import dropbox.client

import logging
logging.basicConfig(level=logging.INFO)
log = logging.getLogger("Dropbox")

__author__ = 'andrey'

class Dropbox(object):
    DROPBOX_FOLDER = "/guidem/"

    def __init__(self):
        configure_path = os.path.join(os.path.split(os.path.abspath( __file__ ))[0], "configure.ini")
        configuration = dropbox.auth.Authenticator.load_config(configure_path)
        authenticator = dropbox.auth.Authenticator(configuration)
        access_token = authenticator.obtain_trusted_access_token(configuration["testing_user"],\
                                                                 configuration["testing_password"])
        self._client = dropbox.client.DropboxClient(configuration["server"],\
                                              configuration["content_server"],\
                                              configuration["port"],\
                                              authenticator,\
                                              access_token)
    def remove_file(self, fname):
        path = os.path.join(self.DROPBOX_FOLDER, fname)
        return True if self._client.file_delete("dropbox", path).status==200 else False

    def get_file(self, fname):
        path = os.path.join(self.DROPBOX_FOLDER, fname)
        response = self._client.get_file("dropbox", path)
        if response.status==200:
            log.info("Get file: '%s'"%path)
            return response.fp
        else:
            log.error("Cannot get file: '%s'"%path)
            return False

    def list_dropbox_directory(self):
        files = self._client.metadata("dropbox", self.DROPBOX_FOLDER)
        files = [os.path.basename(fp["path"]).encode('utf-8') for fp in files.data["contents"]\
                 if fp["path"][-4:]==".mp3"]
        return files
